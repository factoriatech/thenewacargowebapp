﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class seedftl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 40m, 0.0, 149.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 50m, 150.0, 249.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 65m, 250.0, 449.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 10000.0, 85m, 450.0, 800.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 20000.0, 105m, 801.0, 1200.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 40000.0, 120m, 1201.0, 1400.0 });

            migrationBuilder.InsertData(
                table: "FullTruckLoads",
                columns: new[] { "Id", "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 7, 60000.0, 150m, 1401.0, 2500.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 85m, 450.0, 800.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 105m, 801.0, 1200.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 120m, 1201.0, 1400.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 60000.0, 150m, 1401.0, 2500.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 5000.0, 45m, 201.0, 3449.0 });

            migrationBuilder.UpdateData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 8000.0, 65m, 0.0, 200.0 });
        }
    }
}

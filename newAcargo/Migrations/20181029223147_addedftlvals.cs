﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class addedftlvals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "FullTruckLoads",
                columns: new[] { "Id", "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 5, 5000.0, 45m, 201.0, 3449.0 });

            migrationBuilder.InsertData(
                table: "FullTruckLoads",
                columns: new[] { "Id", "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[] { 6, 8000.0, 65m, 0.0, 200.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "FullTruckLoads",
                keyColumn: "Id",
                keyValue: 6);
        }
    }
}

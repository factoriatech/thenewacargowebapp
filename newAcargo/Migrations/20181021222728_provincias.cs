﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class provincias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Provincias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Capital = table.Column<string>(nullable: true),
                    NombreProvincia = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provincias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FactoresProvincias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Origen = table.Column<double>(nullable: false),
                    Destino = table.Column<double>(nullable: false),
                    ProvinciaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactoresProvincias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FactoresProvincias_Provincias_ProvinciaId",
                        column: x => x.ProvinciaId,
                        principalTable: "Provincias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Provincias",
                columns: new[] { "Id", "Capital", "NombreProvincia" },
                values: new object[,]
                {
                    { 1, "Azua", "Azua de Compostela" },
                    { 30, "Santiago Rodríguez", "Sabaneta" },
                    { 29, "Santiago", "Santiago de los Caballeros" },
                    { 28, "San Pedro de Macorís", "San Pedro de Macorís" },
                    { 27, "San Juan", "San Juan de la Maguana" },
                    { 26, "San José de Ocoa", "San José de Ocoa" },
                    { 25, "San Cristóbal", "San Cristóbal" },
                    { 24, "Sánchez Ramírez", "Cotuí" },
                    { 23, "Samaná", "Santa Bárbara de Samaná" },
                    { 22, "Puerto Plata", "San Felipe de Puerto Plata" },
                    { 21, "Peravia", "Baní" },
                    { 20, "Pedernales", "Pedernales" },
                    { 19, "Monte Plata", "Monte Plata" },
                    { 18, "Monte Cristi", "San Fernando de Monte Cristi" },
                    { 17, "Monseñor Nouel", "Bonao" },
                    { 16, "María Trinidad Sánchez", "Nagua" },
                    { 15, "La Vega", "Concepción de la Vega" },
                    { 14, "La Romana", "La Romana" },
                    { 13, "La Altagracia", "Salvaleón de Higüey" },
                    { 12, "Independencia", "Jimaní" },
                    { 11, "Hermanas Mirabal", "Salcedo" },
                    { 10, "Hato Mayor", "Hato Mayor del Rey" },
                    { 9, "Espaillat", "Moca" },
                    { 8, "El Seibo", "Santa Cruz del Seibo" },
                    { 7, "Elías Piña", "Comendador" },
                    { 6, "Duarte", "San Francisco de Macorís" },
                    { 5, "Distrito Nacional", "Santo Domingo" },
                    { 4, "Dajabón", "Dajabón" },
                    { 3, "Barahona", "Santa Cruz de Barahona" },
                    { 2, "Bahoruco", "Neiba" },
                    { 31, "Santo Domingo", "Santo Domingo Este" },
                    { 32, "Valverde", "Mao" }
                });

            migrationBuilder.InsertData(
                table: "FactoresProvincias",
                columns: new[] { "Id", "Destino", "Origen", "ProvinciaId" },
                values: new object[,]
                {
                    { 1, 0.6, 0.4, 1 },
                    { 30, 0.5, 0.45, 30 },
                    { 29, 0.35, 0.35, 29 },
                    { 28, 0.5, 0.45, 28 },
                    { 27, 0.65, 0.65, 27 },
                    { 26, 0.65, 0.4, 26 },
                    { 25, 0.65, 0.4, 25 },
                    { 24, 0.4, 0.5, 24 },
                    { 23, 0.55, 0.45, 23 },
                    { 22, 0.35, 0.35, 22 },
                    { 21, 0.65, 0.4, 21 },
                    { 20, 0.65, 0.65, 20 },
                    { 19, 0.65, 0.65, 19 },
                    { 18, 0.4, 0.5, 18 },
                    { 17, 0.4, 0.5, 17 },
                    { 16, 0.65, 0.45, 16 },
                    { 15, 0.45, 0.35, 15 },
                    { 14, 0.5, 0.35, 14 },
                    { 13, 0.5, 0.35, 13 },
                    { 12, 0.65, 0.65, 12 },
                    { 11, 0.55, 0.45, 11 },
                    { 10, 0.55, 0.55, 10 },
                    { 9, 0.55, 0.55, 9 },
                    { 8, 0.65, 0.65, 8 },
                    { 7, 0.65, 0.65, 7 },
                    { 6, 0.55, 0.55, 6 },
                    { 5, 0.35, 0.35, 5 },
                    { 4, 0.6, 0.5, 4 },
                    { 3, 0.65, 0.4, 3 },
                    { 2, 0.65, 0.4, 2 },
                    { 31, 0.35, 0.45, 31 },
                    { 32, 0.45, 0.4, 32 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_FactoresProvincias_ProvinciaId",
                table: "FactoresProvincias",
                column: "ProvinciaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FactoresProvincias");

            migrationBuilder.DropTable(
                name: "Provincias");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class addedcalcclasses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalculationParameterSets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    FactorDivisor = table.Column<decimal>(nullable: false),
                    PricePickup = table.Column<decimal>(nullable: false),
                    PriceDelivery = table.Column<decimal>(nullable: false),
                    ComisionPorcentage = table.Column<decimal>(nullable: false),
                    MinimumPrice = table.Column<decimal>(nullable: false),
                    MinimumDistance = table.Column<decimal>(nullable: false),
                    VolumeWeightDivisor = table.Column<decimal>(nullable: false),
                    FtlPerKm = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalculationParameterSets", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CalculationParameterSets");
        }
    }
}

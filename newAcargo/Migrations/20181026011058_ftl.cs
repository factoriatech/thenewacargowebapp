﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class ftl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FullTruckLoads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ValorDesde = table.Column<double>(nullable: false),
                    ValorHasta = table.Column<double>(nullable: false),
                    PesoMaximo = table.Column<double>(nullable: false),
                    PrecioPorKm = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FullTruckLoads", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "FullTruckLoads",
                columns: new[] { "Id", "PesoMaximo", "PrecioPorKm", "ValorDesde", "ValorHasta" },
                values: new object[,]
                {
                    { 1, 10000.0, 85m, 450.0, 800.0 },
                    { 2, 20000.0, 105m, 801.0, 1200.0 },
                    { 3, 40000.0, 120m, 1201.0, 1400.0 },
                    { 4, 60000.0, 150m, 1401.0, 2500.0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FullTruckLoads");
        }
    }
}

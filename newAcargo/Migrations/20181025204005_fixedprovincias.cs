﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class fixedprovincias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Provincia",
                table: "Direcciones");

            migrationBuilder.AddColumn<int>(
                name: "ProvinciaId",
                table: "Direcciones",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "CalculationParameterSets",
                keyColumn: "Id",
                keyValue: 1,
                column: "Protected",
                value: true);

            migrationBuilder.CreateIndex(
                name: "IX_Direcciones_ProvinciaId",
                table: "Direcciones",
                column: "ProvinciaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Direcciones_Provincias_ProvinciaId",
                table: "Direcciones",
                column: "ProvinciaId",
                principalTable: "Provincias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Direcciones_Provincias_ProvinciaId",
                table: "Direcciones");

            migrationBuilder.DropIndex(
                name: "IX_Direcciones_ProvinciaId",
                table: "Direcciones");

            migrationBuilder.DropColumn(
                name: "ProvinciaId",
                table: "Direcciones");

            migrationBuilder.AddColumn<string>(
                name: "Provincia",
                table: "Direcciones",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "CalculationParameterSets",
                keyColumn: "Id",
                keyValue: 1,
                column: "Protected",
                value: false);
        }
    }
}

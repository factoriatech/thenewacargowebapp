﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class iniciales : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContactosDeEnvio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Apellido = table.Column<string>(nullable: true),
                    Cedula = table.Column<string>(nullable: true),
                    Apodo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactosDeEnvio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EstatusOrdenes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Estatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstatusOrdenes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NCFTipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TipoDeComprobante = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NCFTipos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Seguros",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Margen = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seguros", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShippingCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Publicado = table.Column<bool>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShippingCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiemposDeEntrega",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    CtdDeHoras = table.Column<int>(nullable: false),
                    Incremento = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiemposDeEntrega", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposDeDireccion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tipo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposDeDireccion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposDeEmpaque",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Empaque = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposDeEmpaque", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Unidades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UnidadDeMedida = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unidades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Direcciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Direccion1 = table.Column<string>(nullable: true),
                    Direccion2 = table.Column<string>(nullable: true),
                    Ciudad = table.Column<string>(nullable: true),
                    Provincia = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Pais = table.Column<string>(nullable: true),
                    Predeterminada = table.Column<bool>(nullable: false),
                    TipodeDireccionId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Direcciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Direcciones_TiposDeDireccion_TipodeDireccionId",
                        column: x => x.TipodeDireccionId,
                        principalTable: "TiposDeDireccion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContenidosDeCarga",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Contenido = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Ancho = table.Column<double>(nullable: false),
                    Alto = table.Column<double>(nullable: false),
                    Profundidad = table.Column<double>(nullable: false),
                    Peso = table.Column<double>(nullable: false),
                    UnidadId = table.Column<int>(nullable: true),
                    ShippingCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContenidosDeCarga", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContenidosDeCarga_ShippingCategories_ShippingCategoryId",
                        column: x => x.ShippingCategoryId,
                        principalTable: "ShippingCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContenidosDeCarga_Unidades_UnidadId",
                        column: x => x.UnidadId,
                        principalTable: "Unidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    TelefonoOficina = table.Column<string>(nullable: true),
                    Extension = table.Column<string>(nullable: true),
                    Cedula = table.Column<string>(nullable: true),
                    Posicion = table.Column<string>(nullable: true),
                    DireccionId = table.Column<int>(nullable: true),
                    Tipo = table.Column<string>(nullable: true),
                    TelefonoCelular = table.Column<string>(nullable: true),
                    Empresa = table.Column<string>(nullable: true),
                    RNC = table.Column<string>(nullable: true),
                    NCFId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Direcciones_DireccionId",
                        column: x => x.DireccionId,
                        principalTable: "Direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_NCFTipos_NCFId",
                        column: x => x.NCFId,
                        principalTable: "NCFTipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EstatusId = table.Column<int>(nullable: true),
                    OrigenId = table.Column<int>(nullable: true),
                    DestinoId = table.Column<int>(nullable: true),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEntrega = table.Column<DateTime>(nullable: false),
                    SeguroId = table.Column<int>(nullable: true),
                    CostoMercancia = table.Column<decimal>(nullable: false),
                    Distance = table.Column<double>(nullable: false),
                    Duration = table.Column<TimeSpan>(nullable: false),
                    ShipperId = table.Column<string>(nullable: true),
                    CarrierId = table.Column<string>(nullable: true),
                    ChoferId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Direcciones_DestinoId",
                        column: x => x.DestinoId,
                        principalTable: "Direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_EstatusOrdenes_EstatusId",
                        column: x => x.EstatusId,
                        principalTable: "EstatusOrdenes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Direcciones_OrigenId",
                        column: x => x.OrigenId,
                        principalTable: "Direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Seguros_SeguroId",
                        column: x => x.SeguroId,
                        principalTable: "Seguros",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cargas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContenidoId = table.Column<int>(nullable: true),
                    Profundidad = table.Column<double>(nullable: false),
                    Ancho = table.Column<double>(nullable: false),
                    Alto = table.Column<double>(nullable: false),
                    Peso = table.Column<double>(nullable: false),
                    Cantidad = table.Column<int>(nullable: false),
                    UnidadId = table.Column<int>(nullable: true),
                    Apilable = table.Column<bool>(nullable: false),
                    EmpaqueId = table.Column<int>(nullable: true),
                    PesoVolumen = table.Column<double>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cargas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cargas_ContenidosDeCarga_ContenidoId",
                        column: x => x.ContenidoId,
                        principalTable: "ContenidosDeCarga",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cargas_TiposDeEmpaque_EmpaqueId",
                        column: x => x.EmpaqueId,
                        principalTable: "TiposDeEmpaque",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cargas_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cargas_Unidades_UnidadId",
                        column: x => x.UnidadId,
                        principalTable: "Unidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DatosDeEnvios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContactoRecogidaId = table.Column<int>(nullable: true),
                    EntregaId = table.Column<int>(nullable: true),
                    FechaInicialRecogida = table.Column<DateTime>(nullable: false),
                    FechaFinalRecogida = table.Column<DateTime>(nullable: false),
                    HorarioRecogida = table.Column<string>(nullable: true),
                    RampaRecogida = table.Column<bool>(nullable: false),
                    LLamarRecogida = table.Column<bool>(nullable: false),
                    SolicitarAccesoRecogida = table.Column<bool>(nullable: false),
                    HorarioDeRecogida = table.Column<string>(nullable: true),
                    DatosContactoEntrega = table.Column<string>(nullable: true),
                    RampaEntrega = table.Column<bool>(nullable: false),
                    LLamarEntrega = table.Column<bool>(nullable: false),
                    SolicitarAccesoEntrega = table.Column<bool>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatosDeEnvios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatosDeEnvios_ContactosDeEnvio_ContactoRecogidaId",
                        column: x => x.ContactoRecogidaId,
                        principalTable: "ContactosDeEnvio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DatosDeEnvios_ContactosDeEnvio_EntregaId",
                        column: x => x.EntregaId,
                        principalTable: "ContactosDeEnvio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DatosDeEnvios_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetallesFinancieros",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Subtotal = table.Column<decimal>(nullable: false),
                    Itbis = table.Column<decimal>(nullable: false),
                    IngresoCarrier = table.Column<decimal>(nullable: false),
                    ComisionAcargo = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetallesFinancieros", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DetallesFinancieros_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "EstatusOrdenes",
                columns: new[] { "Id", "Estatus" },
                values: new object[,]
                {
                    { 5, "Completada" },
                    { 4, "En Ruta" },
                    { 3, "Programada" },
                    { 2, "En Proceso" },
                    { 1, "Borrador" }
                });

            migrationBuilder.InsertData(
                table: "Seguros",
                columns: new[] { "Id", "Margen", "Nombre" },
                values: new object[,]
                {
                    { 2, 0m, "No Asegurar" },
                    { 1, 0.01m, "100% Cobertura (1% Valor)" }
                });

            migrationBuilder.InsertData(
                table: "ShippingCategories",
                columns: new[] { "Id", "Description", "Image", "Nombre", "Publicado" },
                values: new object[,]
                {
                    { 5, null, "cargas.png", "Cargas", false },
                    { 1, null, "materiales.png", "Materiales de Contruccion", false },
                    { 2, null, "alimentos.png", "Alimentos", false },
                    { 3, null, "animales.png", "Animales", false },
                    { 4, null, "hogar.png", "Articulos del Hogar", false },
                    { 8, null, "otros.png", "Otros", false },
                    { 6, null, "piezas-vehiculos.png", "Piezas de Vehiculos", false },
                    { 7, null, "tecnologia.png", "Tecnologia", false }
                });

            migrationBuilder.InsertData(
                table: "TiemposDeEntrega",
                columns: new[] { "Id", "CtdDeHoras", "Incremento", "Nombre" },
                values: new object[,]
                {
                    { 2, 24, 0.5m, "Express" },
                    { 1, 72, 0m, "Standard" }
                });

            migrationBuilder.InsertData(
                table: "TiposDeDireccion",
                columns: new[] { "Id", "Tipo" },
                values: new object[,]
                {
                    { 2, "Destino" },
                    { 1, "Origen" }
                });

            migrationBuilder.InsertData(
                table: "TiposDeEmpaque",
                columns: new[] { "Id", "Empaque" },
                values: new object[,]
                {
                    { 4, "Fundas" },
                    { 3, "Paletas" },
                    { 2, "Cajas" },
                    { 1, "Sacos" }
                });

            migrationBuilder.InsertData(
                table: "Unidades",
                columns: new[] { "Id", "UnidadDeMedida" },
                values: new object[,]
                {
                    { 2, "pulg" },
                    { 1, "cm" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_DireccionId",
                table: "AspNetUsers",
                column: "DireccionId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_NCFId",
                table: "AspNetUsers",
                column: "NCFId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cargas_ContenidoId",
                table: "Cargas",
                column: "ContenidoId");

            migrationBuilder.CreateIndex(
                name: "IX_Cargas_EmpaqueId",
                table: "Cargas",
                column: "EmpaqueId");

            migrationBuilder.CreateIndex(
                name: "IX_Cargas_OrderId",
                table: "Cargas",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Cargas_UnidadId",
                table: "Cargas",
                column: "UnidadId");

            migrationBuilder.CreateIndex(
                name: "IX_ContenidosDeCarga_ShippingCategoryId",
                table: "ContenidosDeCarga",
                column: "ShippingCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ContenidosDeCarga_UnidadId",
                table: "ContenidosDeCarga",
                column: "UnidadId");

            migrationBuilder.CreateIndex(
                name: "IX_DatosDeEnvios_ContactoRecogidaId",
                table: "DatosDeEnvios",
                column: "ContactoRecogidaId");

            migrationBuilder.CreateIndex(
                name: "IX_DatosDeEnvios_EntregaId",
                table: "DatosDeEnvios",
                column: "EntregaId");

            migrationBuilder.CreateIndex(
                name: "IX_DatosDeEnvios_OrderId",
                table: "DatosDeEnvios",
                column: "OrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DetallesFinancieros_OrderId",
                table: "DetallesFinancieros",
                column: "OrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Direcciones_TipodeDireccionId",
                table: "Direcciones",
                column: "TipodeDireccionId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DestinoId",
                table: "Orders",
                column: "DestinoId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_EstatusId",
                table: "Orders",
                column: "EstatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrigenId",
                table: "Orders",
                column: "OrigenId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SeguroId",
                table: "Orders",
                column: "SeguroId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Cargas");

            migrationBuilder.DropTable(
                name: "DatosDeEnvios");

            migrationBuilder.DropTable(
                name: "DetallesFinancieros");

            migrationBuilder.DropTable(
                name: "TiemposDeEntrega");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ContenidosDeCarga");

            migrationBuilder.DropTable(
                name: "TiposDeEmpaque");

            migrationBuilder.DropTable(
                name: "ContactosDeEnvio");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "NCFTipos");

            migrationBuilder.DropTable(
                name: "ShippingCategories");

            migrationBuilder.DropTable(
                name: "Unidades");

            migrationBuilder.DropTable(
                name: "Direcciones");

            migrationBuilder.DropTable(
                name: "EstatusOrdenes");

            migrationBuilder.DropTable(
                name: "Seguros");

            migrationBuilder.DropTable(
                name: "TiposDeDireccion");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class seedcalc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "CalculationParameterSets",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Protected",
                table: "CalculationParameterSets",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CalculationParameterSets",
                columns: new[] { "Id", "Active", "ComisionPorcentage", "FactorDivisor", "FtlPerKm", "MinimumDistance", "MinimumPrice", "Nombre", "PriceDelivery", "PricePickup", "Protected", "VolumeWeightDivisor" },
                values: new object[] { 1, true, 0.3m, 3.0m, 500m, 50m, 3000m, "Standard", 300m, 300m, true, 1m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CalculationParameterSets",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "Protected",
                table: "CalculationParameterSets");

            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "CalculationParameterSets",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace newAcargo.Migrations
{
    public partial class contenido : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EmpaqueId",
                table: "ContenidosDeCarga",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContenidosDeCarga_EmpaqueId",
                table: "ContenidosDeCarga",
                column: "EmpaqueId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContenidosDeCarga_TiposDeEmpaque_EmpaqueId",
                table: "ContenidosDeCarga",
                column: "EmpaqueId",
                principalTable: "TiposDeEmpaque",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContenidosDeCarga_TiposDeEmpaque_EmpaqueId",
                table: "ContenidosDeCarga");

            migrationBuilder.DropIndex(
                name: "IX_ContenidosDeCarga_EmpaqueId",
                table: "ContenidosDeCarga");

            migrationBuilder.DropColumn(
                name: "EmpaqueId",
                table: "ContenidosDeCarga");
        }
    }
}

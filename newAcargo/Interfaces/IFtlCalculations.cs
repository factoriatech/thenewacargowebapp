﻿using newAcargo.Models.Calculations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Interfaces
{
    public interface IFtlCalculations
    {
        Task<decimal> GetFtlPrice(double distance, double peso, double volumen);

        Task<FtlResult> GetFtl(double distance, double peso, double volumen);
    }
}

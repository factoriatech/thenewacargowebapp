﻿using newAcargo.Models.Calculations;
using newAcargo.Models.Cargas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Interfaces
{
    public interface IPriceCalculation
    {
        
        Task<decimal> CalculateComision(int orderId);

        Task<decimal> GetOrderPrice(int orderId);

        Task<decimal> GetPrice(List<Carga> cargas, double distance, int origen, int destino);

        Task<decimal> GetPriceQuote(CargaPriceCalculatorBinding cargaPriceCalculatorBinding, double distance);

        Task<FormulaResult> GetFormulaResult(CargaPriceCalculatorBinding cargaPriceCalculatorBinding, double distance);
       
    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Interfaces
{
    public interface IDireccionesRepository
    {
        Task<DireccionesViewModel> GetDirecciones(string userId);

        Task Add(DireccionViewModel direccion, string userId);

        Task<DireccionViewModel> GetDireccionAsync(int? id);

        Task<List<Direccion>> GetDireccionesOrigenAsync(string userId);

        Task<List<Direccion>> GetDireccionesDestinoAsync(string userId);

        Task EditDireccionAsync(DireccionViewModel direccion, int id);

        Task DeleteDireccionAsync(Direccion direccion);

        Task<List<SelectListItem>> GetProvinciasList();



    }
}

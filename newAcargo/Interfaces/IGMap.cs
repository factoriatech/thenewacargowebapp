﻿using Google.Maps.DistanceMatrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace newAcargo.Interfaces
{
    public interface IGMap
    {
        DistanceMatrixService CreateService();
        Task<string> GetDistance(string origen, string destino);

        Task<string> GetDuration(string origen, string destino);

        Task<double> GetDistanceDouble(string origen, string destino);


        Task<string> GetOrderDistance(int orderId);

        Task<string> GetOrderDuration(int orderId);

        Task<double> GetOrderDistanceDouble(int orderId);

    }
}

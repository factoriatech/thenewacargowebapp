﻿using newAcargo.Models.Cargas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Interfaces
{
    public interface IOrderRepository
    {
        Task<OrderViewModel> ContinueUnfinishedOrder(string userId);

        Task<OrderViewModel> CreateNewOrder(int subCategoryId, string userId);

        Task<bool> UserHasUncompletedOrder(string userId);

        Task<List<Carga>> GetCargasFromOrder(int orderId);
    }
}

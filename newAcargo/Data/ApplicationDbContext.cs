﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using newAcargo.Models;
using newAcargo.Models.Calculations;
using newAcargo.Models.Cargas;
using newAcargo.Models.Direcciones;
using newAcargo.Models.FrontEndUI;
using newAcargo.Models.Orders;

namespace newAcargo.Data
{
    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }


        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<TipoDeDireccion> TiposDeDireccion { get; set; }
        public DbSet<Carga> Cargas { get; set; }
        public DbSet<ContenidoDeCarga> ContenidosDeCarga { get; set; }
        public DbSet<TipoDeEmpaque> TiposDeEmpaque { get; set; }
        public DbSet<Unidad> Unidades { get; set; }
        public DbSet<ContactoDeEnvio> ContactosDeEnvio { get; set; }
        public DbSet<DatosDelEnvio> DatosDeEnvios { get; set; }
        public DbSet<DetalleFinanciero> DetallesFinancieros { get; set; }
        public DbSet<EstatusOrden> EstatusOrdenes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Seguro> Seguros { get; set; }
        public DbSet<TiempoDeEntrega> TiemposDeEntrega { get; set; }
        public DbSet<NCFTipo> NCFTipos { get; set; }
        public DbSet<ShippingCategory> ShippingCategories { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<FactorProvincia> FactoresProvincias { get; set; }
        public DbSet<CalculationParameterSet> CalculationParameterSets { get; set; }
        public DbSet<FullTruckLoad> FullTruckLoads { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Order>()
                .HasOne<DatosDelEnvio>(x => x.DatosDelEnvio)
                .WithOne(o => o.Order)
                .HasForeignKey<DatosDelEnvio>(y => y.OrderId);

            builder.Entity<Order>()
                .HasOne<DetalleFinanciero>(d => d.DetalleFinanciero)
                .WithOne(t => t.Order)
                .HasForeignKey<DetalleFinanciero>(m => m.OrderId);

            builder.Entity<TipoDeDireccion>().HasData(
                new TipoDeDireccion { Id = 1, Tipo = "Origen" },
                new TipoDeDireccion { Id = 2, Tipo = "Destino" }
                );

            builder.Entity<EstatusOrden>().HasData(
                new EstatusOrden { Id = 1, Estatus = "Borrador" },
                new EstatusOrden { Id = 2, Estatus = "En Proceso" },
                new EstatusOrden { Id = 3, Estatus = "Programada" },
                new EstatusOrden { Id = 4, Estatus = "En Ruta" },
                new EstatusOrden { Id = 5, Estatus = "Completada" }
                );

            builder.Entity<TiempoDeEntrega>().HasData(
                new TiempoDeEntrega { Id = 1, Nombre = "Standard", CtdDeHoras = 72, Incremento = 0 },
                new TiempoDeEntrega { Id = 2, Nombre = "Express", CtdDeHoras = 24, Incremento = 0.5M }
                );

            builder.Entity<Seguro>().HasData(
                new Seguro { Id = 1, Nombre = "100% Cobertura (1% Valor)", Margen = 0.01M },
                new Seguro { Id = 2, Nombre = "No Asegurar", Margen = 0 }
                );

            builder.Entity<Unidad>().HasData(
                new Unidad { Id = 1, UnidadDeMedida = "cm" },
                new Unidad { Id = 2, UnidadDeMedida = "pulg" }
                );

            builder.Entity<TipoDeEmpaque>().HasData(
                new TipoDeEmpaque { Id = 1, Empaque = "Sacos" },
                new TipoDeEmpaque { Id = 2, Empaque = "Cajas" },
                new TipoDeEmpaque { Id = 3, Empaque = "Paletas" },
                new TipoDeEmpaque { Id = 4, Empaque = "Fundas" }
                );

            builder.Entity<ShippingCategory>().HasData(
                new ShippingCategory { Id = 1, Nombre = "Materiales de Contruccion", Image = "materiales.png"},
                new ShippingCategory { Id = 2, Nombre = "Alimentos", Image = "alimentos.png" },
                new ShippingCategory { Id = 3, Nombre = "Animales", Image = "animales.png" },
                new ShippingCategory { Id = 4, Nombre = "Articulos del Hogar", Image = "hogar.png" },
                new ShippingCategory { Id = 5, Nombre = "Cargas", Image = "cargas.png" },
                new ShippingCategory { Id = 6, Nombre = "Piezas de Vehiculos", Image = "piezas-vehiculos.png" },
                new ShippingCategory { Id = 7, Nombre = "Tecnologia", Image = "tecnologia.png" },
                new ShippingCategory { Id = 8, Nombre = "Otros", Image = "otros.png" }
                );

            builder.Entity<Provincia>().HasData(
                new Provincia { Id = 1, Capital = "Azua", NombreProvincia = "Azua de Compostela" },
                new Provincia { Id = 2, Capital = "Bahoruco", NombreProvincia = "Neiba" },
                new Provincia { Id = 3, Capital = "Barahona", NombreProvincia = "Santa Cruz de Barahona" },
                new Provincia { Id = 4, Capital = "Dajabón", NombreProvincia = "Dajabón" },
                new Provincia { Id = 5, Capital = "Distrito Nacional", NombreProvincia = "Santo Domingo" },
                new Provincia { Id = 6, Capital = "Duarte", NombreProvincia = "San Francisco de Macorís" },
                new Provincia { Id = 7, Capital = "Elías Piña", NombreProvincia = "Comendador" },
                new Provincia { Id = 8, Capital = "El Seibo", NombreProvincia = "Santa Cruz del Seibo" },
                new Provincia { Id = 9, Capital = "Espaillat", NombreProvincia = "Moca" },
                new Provincia { Id = 10, Capital = "Hato Mayor", NombreProvincia = "Hato Mayor del Rey" },
                new Provincia { Id = 11, Capital = "Hermanas Mirabal", NombreProvincia = "Salcedo" },
                new Provincia { Id = 12, Capital = "Independencia", NombreProvincia = "Jimaní" },
                new Provincia { Id = 13, Capital = "La Altagracia", NombreProvincia = "Salvaleón de Higüey" },
                new Provincia { Id = 14, Capital = "La Romana", NombreProvincia = "La Romana" },
                new Provincia { Id = 15, Capital = "La Vega", NombreProvincia = "Concepción de la Vega" },
                new Provincia { Id = 16, Capital = "María Trinidad Sánchez", NombreProvincia = "Nagua" },
                new Provincia { Id = 17, Capital = "Monseñor Nouel", NombreProvincia = "Bonao" },
                new Provincia { Id = 18, Capital = "Monte Cristi", NombreProvincia = "San Fernando de Monte Cristi" },
                new Provincia { Id = 19, Capital = "Monte Plata", NombreProvincia = "Monte Plata" },
                new Provincia { Id = 20, Capital = "Pedernales", NombreProvincia = "Pedernales" },
                new Provincia { Id = 21, Capital = "Peravia", NombreProvincia = "Baní" },
                new Provincia { Id = 22, Capital = "Puerto Plata", NombreProvincia = "San Felipe de Puerto Plata" },
                new Provincia { Id = 23, Capital = "Samaná", NombreProvincia = "Santa Bárbara de Samaná" },
                new Provincia { Id = 24, Capital = "Sánchez Ramírez", NombreProvincia = "Cotuí" },
                new Provincia { Id = 25, Capital = "San Cristóbal", NombreProvincia = "San Cristóbal" },
                new Provincia { Id = 26, Capital = "San José de Ocoa", NombreProvincia = "San José de Ocoa" },
                new Provincia { Id = 27, Capital = "San Juan", NombreProvincia = "San Juan de la Maguana" },
                new Provincia { Id = 28, Capital = "San Pedro de Macorís", NombreProvincia = "San Pedro de Macorís" },
                new Provincia { Id = 29, Capital = "Santiago", NombreProvincia = "Santiago de los Caballeros" },
                new Provincia { Id = 30, Capital = "Santiago Rodríguez", NombreProvincia = "Sabaneta" },
                new Provincia { Id = 31, Capital = "Santo Domingo", NombreProvincia = "Santo Domingo Este" },
                new Provincia { Id = 32, Capital = "Valverde", NombreProvincia = "Mao" }
            );

            builder.Entity<FactorProvincia>().HasData(
                new FactorProvincia { Id = 1, ProvinciaId = 1, Origen = 0.4, Destino = 0.6 },
                new FactorProvincia { Id = 2, ProvinciaId = 2, Origen = 0.4, Destino = 0.65 },
                new FactorProvincia { Id = 3, ProvinciaId = 3, Origen = 0.4, Destino = 0.65 },
                new FactorProvincia { Id = 4, ProvinciaId = 4, Origen = 0.5, Destino = 0.6 },
                new FactorProvincia { Id = 5, ProvinciaId = 5, Origen = 0.35, Destino = 0.35 },
                new FactorProvincia { Id = 6, ProvinciaId = 6, Origen = 0.55, Destino = 0.55 },
                new FactorProvincia { Id = 7, ProvinciaId = 7, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 8, ProvinciaId = 8, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 9, ProvinciaId = 9, Origen = 0.55, Destino = 0.55 },
                new FactorProvincia { Id = 10, ProvinciaId = 10, Origen = 0.55, Destino = 0.55 },
                new FactorProvincia { Id = 11, ProvinciaId = 11, Origen = 0.45, Destino = 0.55 },
                new FactorProvincia { Id = 12, ProvinciaId = 12, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 13, ProvinciaId = 13, Origen = 0.35, Destino = 0.5 },
                new FactorProvincia { Id = 14, ProvinciaId = 14, Origen = 0.35, Destino = 0.5 },
                new FactorProvincia { Id = 15, ProvinciaId = 15, Origen = 0.35, Destino = 0.45 },
                new FactorProvincia { Id = 16, ProvinciaId = 16, Origen = 0.45, Destino = 0.65 },
                new FactorProvincia { Id = 17, ProvinciaId = 17, Origen = 0.5, Destino = 0.4 },
                new FactorProvincia { Id = 18, ProvinciaId = 18, Origen = 0.5, Destino = 0.4 },
                new FactorProvincia { Id = 19, ProvinciaId = 19, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 20, ProvinciaId = 20, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 21, ProvinciaId = 21, Origen = 0.4, Destino = 0.65 },
                new FactorProvincia { Id = 22, ProvinciaId = 22, Origen = 0.35, Destino = 0.35 },
                new FactorProvincia { Id = 23, ProvinciaId = 23, Origen = 0.45, Destino = 0.55 },
                new FactorProvincia { Id = 24, ProvinciaId = 24, Origen = 0.5, Destino = 0.4 },
                new FactorProvincia { Id = 25, ProvinciaId = 25, Origen = 0.4, Destino = 0.65 },
                new FactorProvincia { Id = 26, ProvinciaId = 26, Origen = 0.4, Destino = 0.65 },
                new FactorProvincia { Id = 27, ProvinciaId = 27, Origen = 0.65, Destino = 0.65 },
                new FactorProvincia { Id = 28, ProvinciaId = 28, Origen = 0.45, Destino = 0.5 },
                new FactorProvincia { Id = 29, ProvinciaId = 29, Origen = 0.35, Destino = 0.35 },
                new FactorProvincia { Id = 30, ProvinciaId = 30, Origen = 0.45, Destino = 0.5 },
                new FactorProvincia { Id = 31, ProvinciaId = 31, Origen = 0.45, Destino = 0.35 },
                new FactorProvincia { Id = 32, ProvinciaId = 32, Origen = 0.4, Destino = 0.45 }
            );

            builder.Entity<CalculationParameterSet>().HasData(
                new CalculationParameterSet
                {
                    Id = 1,
                    Active = true,
                    Nombre = "Standard",
                    ComisionPorcentage = 0.3M,
                    Protected = true,
                    FactorDivisor = 3.0M,
                    PricePickup = 300M,
                    PriceDelivery = 300M,
                    VolumeWeightDivisor = 1M,
                    FtlPerKm = 500M,
                    MinimumDistance = 50M,
                    MinimumPrice = 3000M
                });

            builder.Entity<FullTruckLoad>().HasData(
                new FullTruckLoad { Id = 1, ValorDesde = 0, ValorHasta = 149, PesoMaximo = 10000, PrecioPorKm = 40 },
                new FullTruckLoad { Id = 2, ValorDesde = 150, ValorHasta = 249, PesoMaximo = 20000, PrecioPorKm = 50 },
                new FullTruckLoad { Id = 3, ValorDesde = 250, ValorHasta = 449, PesoMaximo = 40000, PrecioPorKm = 65 },
                new FullTruckLoad { Id = 4, ValorDesde = 450, ValorHasta = 800, PesoMaximo = 10000, PrecioPorKm = 85 },
                new FullTruckLoad { Id = 5, ValorDesde = 801, ValorHasta = 1200, PesoMaximo = 20000, PrecioPorKm = 105 },
                new FullTruckLoad { Id = 6, ValorDesde = 1201, ValorHasta = 1400, PesoMaximo = 40000, PrecioPorKm = 120 },
                new FullTruckLoad { Id = 7, ValorDesde = 1401, ValorHasta = 2500, PesoMaximo = 60000, PrecioPorKm = 150 }
                );
        }

    }
}

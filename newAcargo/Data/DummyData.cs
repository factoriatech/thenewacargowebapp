﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using newAcargo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager
            )
        {
            context.Database.EnsureCreated();

           

            string role1 = "Admin";
            string desc1 = "This is the administrator role";

            string role2 = "Shippers";
            string desc2 = "People who will aquire shipping services";

            string role3 = "Carriers";
            string desc3 = "Companies or Individuals who will provide shipping services";

            string role4 = "Manager";
            string desc4 = "Business Owner";

            string role5 = "Employee";
            string desc5 = "Business Employees";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role3) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role3, desc3, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role4) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role4, desc4, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role5) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role5, desc5, DateTime.Now));
            }
        }
    }
}

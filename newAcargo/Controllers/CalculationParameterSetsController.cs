﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Models.Calculations;

namespace newAcargo.Controllers
{
    public class CalculationParameterSetsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CalculationParameterSetsController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> ActivateStandard()
        {
            var active = await _context
                .CalculationParameterSets
                .SingleOrDefaultAsync(x => x.Active == true);
            active.Active = false;
            _context.Update(active);
            var std = await _context.CalculationParameterSets.SingleOrDefaultAsync(x => x.Protected == true);
            std.Active = true;
            _context.Update(std);
            await _context.SaveChangesAsync();
            

            return RedirectToAction(nameof(Index));

        }
        // GET: CalculationParameterSets
        public async Task<IActionResult> Index()
        {
            var sets = await _context.CalculationParameterSets.ToListAsync();
            var active = sets.Where(x => x.Active == true).FirstOrDefault();
            ViewBag.Nombre = active.Nombre;
            ViewBag.PricePickup = active.PricePickup;
            ViewBag.PriceDelivery = active.PriceDelivery;
            ViewBag.MinimumDistance = active.MinimumDistance;
            ViewBag.VolumeWeightDivisor = active.VolumeWeightDivisor;
            ViewBag.MinimumPrice = active.MinimumPrice;
            ViewBag.FactorDivisor = active.FactorDivisor;
            ViewBag.ComisionPorcentage = active.ComisionPorcentage;
            ViewBag.activeSet = true;
            ViewBag.Protected = active.Protected;
            return View(await _context
                .CalculationParameterSets
                .Where(y => y.Active == false)
                .ToListAsync());
        }

        // GET: CalculationParameterSets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calculationParameterSet = await _context.CalculationParameterSets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calculationParameterSet == null)
            {
                return NotFound();
            }

            return View(calculationParameterSet);
        }

        // GET: CalculationParameterSets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CalculationParameterSets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,FactorDivisor,PricePickup,PriceDelivery,ComisionPorcentage,MinimumPrice,MinimumDistance,VolumeWeightDivisor,FtlPerKm,Active")] CalculationParameterSet calculationParameterSet)
        {
            if (ModelState.IsValid)
            {
                if(calculationParameterSet.Active == true)
                {
                    var preterminada = await _context
                        .CalculationParameterSets
                        .Where(x => x.Active == true)
                        .FirstOrDefaultAsync();

                    if(preterminada != null)
                    {
                        preterminada.Active = false;
                        _context.Update(preterminada);
                    }
                }
                calculationParameterSet.Protected = false;
                _context.Add(calculationParameterSet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(calculationParameterSet);
        }

        // GET: CalculationParameterSets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calculationParameterSet = await _context.CalculationParameterSets.FindAsync(id);
            if (calculationParameterSet == null)
            {
                return NotFound();
            }
            return View(calculationParameterSet);
        }

        // POST: CalculationParameterSets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,FactorDivisor,PricePickup,PriceDelivery,ComisionPorcentage,MinimumPrice,MinimumDistance,VolumeWeightDivisor,FtlPerKm,Active")] CalculationParameterSet calculationParameterSet)
        {
            if (id != calculationParameterSet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (calculationParameterSet.Active == true)
                {
                    var preterminada = await _context
                        .CalculationParameterSets
                        .Where(x => x.Active == true)
                        .FirstOrDefaultAsync();

                    if (preterminada != null)
                    {
                        preterminada.Active = false;
                        _context.Update(preterminada);
                       
                    }
                }
                else
                {
                    var currentSet = await 
                        _context
                        .CalculationParameterSets
                        .SingleOrDefaultAsync(t => t.Id == calculationParameterSet.Id);

                    if(currentSet.Active == true)
                    {
                        var standard = await _context
                        .CalculationParameterSets
                        .SingleOrDefaultAsync(g => g.Protected == true);
                        standard.Active = true;
                        _context.Update(standard);
                        
                    }
                }

                calculationParameterSet.Protected = false;
                try
                {
                    _context.Update(calculationParameterSet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CalculationParameterSetExists(calculationParameterSet.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(calculationParameterSet);
        }

        // GET: CalculationParameterSets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calculationParameterSet = await _context.CalculationParameterSets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calculationParameterSet == null)
            {
                return NotFound();
            }

            return View(calculationParameterSet);
        }

        // POST: CalculationParameterSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {

            var calculationParameterSet = await _context.CalculationParameterSets.FindAsync(id);
            if(calculationParameterSet.Protected != true)
            {
                if(calculationParameterSet.Active == true)
                {
                    var standard = await _context
                        .CalculationParameterSets
                        .SingleOrDefaultAsync(g => g.Protected == true);
                    standard.Active = true;
                    _context.Update(standard);
                }
                _context.CalculationParameterSets.Remove(calculationParameterSet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewBag.CannotDeleteProtected = "El Set de Parametros Standard no se puede Borrar";
                return View(calculationParameterSet);
            }
            
        }

        private bool CalculationParameterSetExists(int id)
        {
            return _context.CalculationParameterSets.Any(e => e.Id == id);
        }
    }
}

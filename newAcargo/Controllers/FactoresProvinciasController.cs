﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Models.Calculations;

namespace newAcargo.Controllers
{
    public class FactoresProvinciasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FactoresProvinciasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: FactoresProvincias
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.FactoresProvincias.Include(f => f.Provincia);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: FactoresProvincias/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorProvincia = await _context.FactoresProvincias
                .Include(f => f.Provincia)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (factorProvincia == null)
            {
                return NotFound();
            }

            return View(factorProvincia);
        }

        // GET: FactoresProvincias/Create
        public IActionResult Create()
        {
            ViewData["ProvinciaId"] = new SelectList(_context.Provincias, "Id", "Id");
            return View();
        }

        // POST: FactoresProvincias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Origen,Destino,ProvinciaId")] FactorProvincia factorProvincia)
        {
            if (ModelState.IsValid)
            {
                _context.Add(factorProvincia);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinciaId"] = new SelectList(_context.Provincias, "Id", "Id", factorProvincia.ProvinciaId);
            return View(factorProvincia);
        }

        // GET: FactoresProvincias/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorProvincia = await _context.FactoresProvincias.FindAsync(id);
            if (factorProvincia == null)
            {
                return NotFound();
            }
            var provinciaName = await _context
                .Provincias
                .SingleOrDefaultAsync(x => x.Id == factorProvincia.ProvinciaId);

            ViewBag.Provincia = provinciaName.Capital + ", " + provinciaName.NombreProvincia;


            return View(factorProvincia);
        }

        // POST: FactoresProvincias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Origen,Destino,ProvinciaId")] FactorProvincia factorProvincia)
        {
            if (id != factorProvincia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(factorProvincia);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FactorProvinciaExists(factorProvincia.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinciaId"] = new SelectList(_context.Provincias, "Id", "Id", factorProvincia.ProvinciaId);
            return View(factorProvincia);
        }

        // GET: FactoresProvincias/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorProvincia = await _context.FactoresProvincias
                .Include(f => f.Provincia)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (factorProvincia == null)
            {
                return NotFound();
            }

            return View(factorProvincia);
        }

        // POST: FactoresProvincias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var factorProvincia = await _context.FactoresProvincias.FindAsync(id);
            _context.FactoresProvincias.Remove(factorProvincia);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FactorProvinciaExists(int id)
        {
            return _context.FactoresProvincias.Any(e => e.Id == id);
        }
    }
}

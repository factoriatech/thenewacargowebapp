﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models;
using newAcargo.Models.Cargas;
using newAcargo.Models.FrontEndUI;
using newAcargo.Services;
using static System.Net.WebRequestMethods;

namespace newAcargo.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileProvider fileProvider;
        private readonly IOrderRepository _orderRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public CategoriesController(
            ApplicationDbContext context, 
            IFileProvider fileProvider, 
            IOrderRepository orderRepo,
             UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.fileProvider = fileProvider;
            _orderRepo = orderRepo;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = _userManager.GetUserId(User);
            var hasOrder = await _orderRepo.UserHasUncompletedOrder(user);
            if (hasOrder)
            {
                return RedirectToAction("CompleteNewOrder", "Order");
            }


            ViewBag.ImageUrl = ValueServices.ImageUrl;
            return View(await _context
                        .ShippingCategories
                        .ToListAsync()
                       );
        }

        public async Task<IActionResult> SubcategoriasList()
        {
            var contenidos = await _context
                .ContenidosDeCarga
                .Where(i => i.IsAModel == true)
                .Include(x => x.ShippingCategories)
                .Select( m => new SubcategoryListViewModel()
            {
                Id = m.Id,
                Ancho = m.Ancho,
                Alto = m.Alto,
                Profundidad = m.Profundidad,
                Peso = m.Peso,
                Categoria = m.ShippingCategories.Nombre,
                Contenido = m.Contenido
            }).ToListAsync();

            return View(contenidos);
        }
        
        public async Task<IActionResult> SubCategories(int id)
        {
            var subcategorias = await _context
                .ContenidosDeCarga
                .Where(x => x.ShippingCategoryId == id && x.IsAModel == true)
                .ToListAsync();

            ViewBag.ImageUrl = ValueServices.ImageUrl;
            ViewBag.Id = id;

            return View(subcategorias);
        }

        public async Task<IActionResult> CrearSubcategoria()
        {
            var subcateogryViewModel = new SubcategoryViewModel
            {
                Categories = await _context.ShippingCategories.Select(
                o => new SelectListItem()
                {
                    Text = o.Nombre,
                    Value = o.Id.ToString()
                }
            ).ToListAsync(),

                Unidades = await _context.Unidades.Select(
                x => new SelectListItem()
                {
                    Text = x.UnidadDeMedida,
                    Value = x.Id.ToString()
                }
            ).ToListAsync(),
                Empaques = await _context.TiposDeEmpaque.Select(
                    w => new SelectListItem()
                    {
                        Text = w.Empaque,
                        Value = w.Id.ToString()
                    }).ToListAsync()
            };
            return View(subcateogryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GuardarSubcategoria([Bind("Contenido,ImageFile,Ancho,Alto,Profundidad,Peso, SelectedUnidadId, SelectedCategoryId, SelectedEmpaque")] SubcategoryViewModel subcategory )
        {
            var archivo = "acargo_logo.png";
            if(ModelState.IsValid)
            {
                if(subcategory.ImageFile != null)
                {
                    var file = subcategory.ImageFile;
                    archivo = file.FileName;
                    var path = Path.Combine(
                           Directory.GetCurrentDirectory(), "wwwroot/images/subcategories",
                           file.FileName);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    } 
                }

                var subCat = new ContenidoDeCarga
                {
                    Alto = subcategory.Alto,
                    Ancho = subcategory.Ancho,
                    Profundidad = subcategory.Profundidad,
                    Peso = subcategory.Peso,
                    Image = archivo,
                    Contenido = subcategory.Contenido,
                    IsAModel = true,

                    Unidad = await _context
                    .Unidades
                    .Where(x => x.Id == subcategory.SelectedUnidadId)
                    .FirstOrDefaultAsync(),

                    ShippingCategoryId = subcategory.SelectedCategoryId,

                    Empaque = await _context
                    .TiposDeEmpaque
                    .Where(f => f.Id == subcategory.SelectedEmpaque)
                    .FirstOrDefaultAsync()
                };

                _context.Add(subCat);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(SubcategoriasList));
            }

            return View(subcategory);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subcateogryViewModel = new SubcategoryViewModel
            {
                Categories = await _context
                    .ShippingCategories
                    .Select(
                o => new SelectListItem()
                {
                    Text = o.Nombre,
                    Value = o.Id.ToString()
                }
            ).ToListAsync(),

                Unidades = await _context
                    .Unidades
                    .Select(
                x => new SelectListItem()
                {
                    Text = x.UnidadDeMedida,
                    Value = x.Id.ToString()
                }
            ).ToListAsync(),

                Empaques = await _context
                .TiposDeEmpaque
                .Select( g => new SelectListItem()
                {
                    Text = g.Empaque,
                    Value = g.Id.ToString()
                }).ToListAsync()
            };

            var contenidoDeCarga = await _context
                .ContenidosDeCarga
                .Include(x => x.Unidad)
                .Include(y => y.Empaque)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (contenidoDeCarga == null)
            {
                return NotFound();
            }
            var unidad = contenidoDeCarga.Unidad;
            var empaque = contenidoDeCarga.Empaque;

            subcateogryViewModel.Id = contenidoDeCarga.Id;
            subcateogryViewModel.Alto = contenidoDeCarga.Alto;
            subcateogryViewModel.Ancho = contenidoDeCarga.Ancho;
            subcateogryViewModel.Profundidad = contenidoDeCarga.Profundidad;
            subcateogryViewModel.Peso = contenidoDeCarga.Peso;
            subcateogryViewModel.Contenido = contenidoDeCarga.Contenido;
            subcateogryViewModel.Image = contenidoDeCarga.Image;
            subcateogryViewModel.SelectedUnidadId = unidad.Id;       
            subcateogryViewModel.SelectedEmpaque = empaque.Id;
            subcateogryViewModel.SelectedCategoryId = contenidoDeCarga.ShippingCategoryId;

            return View(subcateogryViewModel);
        }


        [HttpPost]
        [Route("Categories/Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Contenido,ImageFile,Ancho,Alto,Profundidad,Peso, SelectedUnidadId, SelectedCategoryId,SelectedEmpaque")] SubcategoryViewModel subcategoryViewModel)
        {
            if (id != subcategoryViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var subcat = await _context
                    .ContenidosDeCarga
                    .SingleOrDefaultAsync(x => x.Id == id);

                subcat.Contenido = subcategoryViewModel.Contenido;
                subcat.Alto = subcategoryViewModel.Alto;
                subcat.Ancho = subcategoryViewModel.Ancho;
                subcat.Profundidad = subcategoryViewModel.Profundidad;
                subcat.Peso = subcategoryViewModel.Peso;
                subcat.IsAModel = true;
                subcat.Unidad = await _context
                    .Unidades
                    .SingleOrDefaultAsync(g => g.Id == subcategoryViewModel.SelectedUnidadId);
                subcat.Empaque = await _context
                    .TiposDeEmpaque
                    .SingleOrDefaultAsync(t => t.Id == subcategoryViewModel.SelectedEmpaque);

                subcat.ShippingCategoryId = subcategoryViewModel.SelectedCategoryId;
                subcat.ShippingCategories = await _context
                    .ShippingCategories
                    .SingleOrDefaultAsync(c => c.Id == subcategoryViewModel.SelectedCategoryId);

                try
                {
                    _context.Update(subcat);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContenidoDeCargaExists(subcat.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(subcategoryViewModel);
        }




        public async Task<IActionResult> Delete(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }

            var contenido = await _context
                .ContenidosDeCarga
                .Include(x => x.ShippingCategories)
                .FirstOrDefaultAsync(x => x.Id == id);

            if(contenido == null)
            {
                return NotFound();
            }

            return View(contenido);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contenidoDeCarga = await _context.ContenidosDeCarga.FindAsync(id);
            _context.ContenidosDeCarga.Remove(contenidoDeCarga);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }



        private bool ContenidoDeCargaExists(int id)
        {
            return _context.ContenidosDeCarga.Any(e => e.Id == id);
        }



        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {               
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
            };
        }
    }
}

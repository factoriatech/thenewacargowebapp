﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Models.Cargas;

namespace newAcargo.Controllers
{
    public class TipoDeEmpaquesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoDeEmpaquesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TipoDeEmpaques
        public async Task<IActionResult> Index()
        {
            return View(await _context.TiposDeEmpaque.ToListAsync());
        }

        // GET: TipoDeEmpaques/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDeEmpaque = await _context.TiposDeEmpaque
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoDeEmpaque == null)
            {
                return NotFound();
            }

            return View(tipoDeEmpaque);
        }

        // GET: TipoDeEmpaques/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoDeEmpaques/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Empaque")] TipoDeEmpaque tipoDeEmpaque)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoDeEmpaque);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoDeEmpaque);
        }

        // GET: TipoDeEmpaques/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDeEmpaque = await _context.TiposDeEmpaque.FindAsync(id);
            if (tipoDeEmpaque == null)
            {
                return NotFound();
            }
            return View(tipoDeEmpaque);
        }

        // POST: TipoDeEmpaques/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Empaque")] TipoDeEmpaque tipoDeEmpaque)
        {
            if (id != tipoDeEmpaque.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoDeEmpaque);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoDeEmpaqueExists(tipoDeEmpaque.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoDeEmpaque);
        }

        // GET: TipoDeEmpaques/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDeEmpaque = await _context.TiposDeEmpaque
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoDeEmpaque == null)
            {
                return NotFound();
            }

            return View(tipoDeEmpaque);
        }

        // POST: TipoDeEmpaques/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoDeEmpaque = await _context.TiposDeEmpaque.FindAsync(id);
            _context.TiposDeEmpaque.Remove(tipoDeEmpaque);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoDeEmpaqueExists(int id)
        {
            return _context.TiposDeEmpaque.Any(e => e.Id == id);
        }
    }
}

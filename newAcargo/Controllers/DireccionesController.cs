﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models;
using newAcargo.Models.Direcciones;
using newAcargo.Services;

namespace newAcargo.Controllers
{
    [Authorize(Roles ="Shippers")]
    public class DireccionesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IDireccionesRepository _direcciones;
        private readonly UserManager<ApplicationUser> _userManager;

        public DireccionesController(ApplicationDbContext context, IDireccionesRepository direcciones,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _direcciones = direcciones;
            _userManager = userManager;
        }

        // GET: Direcciones
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            return View(await _direcciones.GetDirecciones(user.Id));
        }

        // GET: Direcciones/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.Provincias = ValueServices.GetProvincias();
            ViewBag.TipoDeDireccion = _context.TiposDeDireccion
                .Select(s => new SelectListItem()
                {
                    Value = s.Tipo,
                    Text = s.Tipo
                }).ToList();
            var direccionViewModel = new DireccionViewModel();
            direccionViewModel.Provincias = await _direcciones.GetProvinciasList();
            return View(direccionViewModel);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TipoDeDireccion,Nombre,Direccion1,Direccion2,Ciudad,SelectedProvincia,Zip,Pais,Predeterminada")] DireccionViewModel direccion )
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);
                await _direcciones.Add(direccion, userId);                
                return RedirectToAction(nameof(Index));
            }
            return View(direccion);
        }

        // GET: Direcciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var direccion = await _direcciones.GetDireccionAsync(id);

            if (direccion == null)
            {
                return NotFound();
            }

            return View(direccion);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Direccion1,Direccion2,Ciudad,SelectedProvincia,Zip,Pais,Predeterminada")] DireccionViewModel direccion)
        {
            if (id != direccion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _direcciones.EditDireccionAsync(direccion, id);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DireccionViewModelExists(direccion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(direccion);
        }

        // GET: Direcciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
           
            var direccion = await _context.Direcciones
                .Include(p => p.Provincia)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (direccion == null)
            {
                return NotFound();
            }

            return View(direccion);
        }

        // POST: Direcciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var direccion = await _context.Direcciones.FindAsync(id);
            await _direcciones.DeleteDireccionAsync(direccion);
            return RedirectToAction(nameof(Index));
        }

        private bool DireccionViewModelExists(int id)
        {
            return _context.Direcciones.Any(e => e.Id == id);
        }
    }
}

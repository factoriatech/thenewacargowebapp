﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models.Calculations;

namespace newAcargo.Controllers
{
    public class PriceCalculatorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IPriceCalculation _price;
        private readonly IGMap _gmap;
        public PriceCalculatorController(ApplicationDbContext context, IPriceCalculation price, IGMap gmap)
        {
            _context = context;
            _price = price;
            _gmap = gmap;
        }


        // GET: Calculator
        public async Task<IActionResult> Index()
        {
            var provincias = await _context.Provincias.Select(x => new SelectListItem
            {
                Text = x.NombreProvincia,
                Value = x.Id.ToString()
            }).OrderBy(m => m.Text).ToListAsync();

            var model = new PriceCalculatorViewModel();
            model.Origen = provincias;
            model.Destino = provincias;
            model.Empaques = await _context.TiposDeEmpaque.Select(x => new SelectListItem
            {
                Text = x.Empaque,
                Value = x.Id.ToString()
            }).ToListAsync();

            return View(model);
        }


        public async Task<IActionResult> Tryouts()
        {
            var provincias = await _context.Provincias.Select(x => new SelectListItem
            {
                Text = x.NombreProvincia,
                Value = x.Id.ToString()
            }).OrderBy(m => m.Text).ToListAsync();

            var model = new PriceCalculatorViewModel();
            model.Origen = provincias;
            model.Destino = provincias;
            model.Empaques = await _context.TiposDeEmpaque.Select(x => new SelectListItem
            {
                Text = x.Empaque,
                Value = x.Id.ToString()
            }).ToListAsync();

            return View(model);
        }




        [HttpPost]
        public async Task<IActionResult> GetPrices(CargaPriceCalculatorBinding cargaPriceCalculatorBinding)
        {
            var origen = await _context
                .Provincias
                .SingleOrDefaultAsync(x => x.Id == cargaPriceCalculatorBinding.ProvinciaOrigen);
            var destino = await _context
                .Provincias
                .SingleOrDefaultAsync(x => x.Id == cargaPriceCalculatorBinding.ProvinciaDestino);

            string origenString = origen.Capital + ", " + origen.NombreProvincia;
            string destinoString = destino.Capital + ", " + destino.NombreProvincia;
            var distance = await _gmap.GetDistanceDouble(origenString, destinoString);
            var _params = await _context
                .CalculationParameterSets
                .Where(x => x.Active == true)
                .FirstOrDefaultAsync();

            var quoteValues = new QuoteValues();
            quoteValues.CargoEntrega = _params.PriceDelivery;
            quoteValues.CargoRecogida = _params.PricePickup;
            quoteValues.Distance = distance;
            quoteValues.SubTotal = await _price.GetPriceQuote(cargaPriceCalculatorBinding, distance);

            return PartialView("QuoteTotals", quoteValues);
        }

        [HttpPost]
        public async Task<IActionResult> RenderFormulaResults(CargaPriceCalculatorBinding cargaPriceCalculatorBinding)
        {
            var origen = await _context
                .Provincias
                .SingleOrDefaultAsync(x => x.Id == cargaPriceCalculatorBinding.ProvinciaOrigen);
            var destino = await _context
                .Provincias
                .SingleOrDefaultAsync(x => x.Id == cargaPriceCalculatorBinding.ProvinciaDestino);

            string origenString = origen.Capital + ", " + origen.NombreProvincia;
            string destinoString = destino.Capital + ", " + destino.NombreProvincia;
            var distance = await _gmap.GetDistanceDouble(origenString, destinoString);
            var _params = await _context
                .CalculationParameterSets
                .Where(x => x.Active == true)
                .FirstOrDefaultAsync();

            var formulaResult = await _price.GetFormulaResult(cargaPriceCalculatorBinding, distance);
            return PartialView("FormulaResults", formulaResult);
        }

        // GET: Calculator/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Calculator/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Calculator/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Calculator/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Calculator/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Calculator/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Calculator/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
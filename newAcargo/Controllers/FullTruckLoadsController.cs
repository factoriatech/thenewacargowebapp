﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Models.Calculations;

namespace newAcargo.Controllers
{
    public class FullTruckLoadsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FullTruckLoadsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: FullTruckLoads
        public async Task<IActionResult> Index()
        {
            return View(await _context.FullTruckLoads.ToListAsync());
        }

        // GET: FullTruckLoads/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fullTruckLoad = await _context.FullTruckLoads
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fullTruckLoad == null)
            {
                return NotFound();
            }

            return View(fullTruckLoad);
        }

        // GET: FullTruckLoads/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FullTruckLoads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ValorDesde,ValorHasta,PesoMaximo,PrecioPorKm")] FullTruckLoad fullTruckLoad)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fullTruckLoad);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(fullTruckLoad);
        }

        // GET: FullTruckLoads/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fullTruckLoad = await _context.FullTruckLoads.FindAsync(id);
            if (fullTruckLoad == null)
            {
                return NotFound();
            }
            return View(fullTruckLoad);
        }

        // POST: FullTruckLoads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ValorDesde,ValorHasta,PesoMaximo,PrecioPorKm")] FullTruckLoad fullTruckLoad)
        {
            if (id != fullTruckLoad.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fullTruckLoad);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FullTruckLoadExists(fullTruckLoad.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(fullTruckLoad);
        }

        // GET: FullTruckLoads/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fullTruckLoad = await _context.FullTruckLoads
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fullTruckLoad == null)
            {
                return NotFound();
            }

            return View(fullTruckLoad);
        }

        // POST: FullTruckLoads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fullTruckLoad = await _context.FullTruckLoads.FindAsync(id);
            _context.FullTruckLoads.Remove(fullTruckLoad);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FullTruckLoadExists(int id)
        {
            return _context.FullTruckLoads.Any(e => e.Id == id);
        }
    }
}

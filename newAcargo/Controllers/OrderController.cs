﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models;
using newAcargo.Models.Cargas;
using newAcargo.Models.Orders;

namespace newAcargo.Controllers
{
    [Authorize(Roles = "Shippers")]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOrderRepository _orderRepo;

        public OrderController(
            ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager,
            IOrderRepository orderRepo)
        {
            _context = context;
            _userManager = userManager;
            _orderRepo = orderRepo;
        }


        public async Task<IActionResult> NewShipment()
        {
            var user = _userManager.GetUserId(User);
            var hasOrder = await _orderRepo.UserHasUncompletedOrder(user);
            if (hasOrder)
            {
                RedirectToAction(nameof(CompleteNewOrder));
            }

            return RedirectToAction("Index", "Categories");
        }



        public async Task<IActionResult> Index(int id)
        {
            var user = _userManager.GetUserId(User);
            var hasOrder = await _orderRepo.UserHasUncompletedOrder(user);

            if (hasOrder)
            {
               RedirectToAction(nameof(CompleteNewOrder));
            }

            return View(await _orderRepo.CreateNewOrder(id, user) );
        }


        public async Task<IActionResult> CompleteNewOrder()
        {

            var user = _userManager.GetUserId(User);
            var hasOrder = await _orderRepo.UserHasUncompletedOrder(user);

            if (hasOrder)
            {
                return View(nameof(Index), await _orderRepo.ContinueUnfinishedOrder(user) );
            }

            return RedirectToAction( nameof(Index) );
        }

        [HttpPost]
        public async Task<PartialViewResult> RenderCargasList(int orderId)
        {
            var model = await _orderRepo.GetCargasFromOrder(orderId);

            return PartialView("_cargasList", model);
        }

        [HttpPost]
        public IActionResult SaveCargaRow(Carga carga, ContenidoDeCarga contenido, int unidad, int empaque)
        {
            var model = new Carga
            {
                Profundidad = carga.Profundidad,
                Alto = carga.Alto,
                Ancho = carga.Ancho,
                Contenido = contenido,

            };


            return PartialView("_cargasList", model);
        }



    }
}

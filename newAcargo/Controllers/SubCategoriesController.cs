﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using newAcargo.Data;


namespace newAcargo.Controllers
{
    public class SubCategoriesController : Controller
    {
        private readonly IFileProvider fileProvider;
        private readonly ApplicationDbContext context;

        public SubCategoriesController(IFileProvider fileProvider, ApplicationDbContext context)
        {
            this.fileProvider = fileProvider;
            this.context = context;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("El archivo no fue seleccionado ");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/images/subcategories",
                        file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return RedirectToAction("Contact","Home");
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {               
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
            };
        }

    }
}
﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Services
{
    public class DireccionesRepository : IDireccionesRepository
    {
        private readonly ApplicationDbContext _context;

        public DireccionesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(DireccionViewModel direccion, string userId)
        {
            var tipo = await _context
                .TiposDeDireccion
                .SingleOrDefaultAsync(x => x.Tipo == direccion.TipoDeDireccion);

            var provincia = await _context
                .Provincias
                .SingleOrDefaultAsync(v => v.Id == direccion.SelectedProvincia);

            var newdir = new Direccion {
                Nombre = direccion.Nombre,
                Direccion1 = direccion.Direccion1,
                Direccion2 = direccion.Direccion2,
                Ciudad = direccion.Ciudad,
                Provincia = provincia,
                UserId = userId,
                Pais = direccion.Pais,
                Zip = direccion.Zip,
                Predeterminada = direccion.Predeterminada,
                TipodeDireccion = tipo
            };

            if(newdir.Predeterminada == true)
            {
                var preterminada = await _context
                    .Direcciones
                    .SingleOrDefaultAsync(p => p.Predeterminada == true 
                    && p.TipodeDireccion == tipo);

                if (preterminada != null)
                {
                    preterminada.Predeterminada = false;
                    _context.Update(preterminada);
                }               
            }
            _context.Add(newdir);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteDireccionAsync(Direccion direccion)
        {
            _context.Remove(direccion);
            await _context.SaveChangesAsync();
        }

        public async Task EditDireccionAsync(DireccionViewModel direccion, int id)
        {
            var editedDireccion = await _context
                .Direcciones
                .Where(x => x.Id == id)
                .Include(m => m.TipodeDireccion)
                .Include(p => p.Provincia)
                .SingleOrDefaultAsync();

            if (direccion.Predeterminada == true)
            {
                if (editedDireccion.Predeterminada == false)
                {
                    var preterminada = await _context
                    .Direcciones
                    .SingleOrDefaultAsync(p => p.Predeterminada == true
                    && p.TipodeDireccion == editedDireccion.TipodeDireccion);

                    if (preterminada != null)
                    {
                        preterminada.Predeterminada = false;
                        _context.Update(preterminada);
                    }
                }
            }

            var provincia = await _context
                .Provincias
                .SingleOrDefaultAsync(v => v.Id == direccion.SelectedProvincia);

            editedDireccion.Nombre = direccion.Nombre;
            editedDireccion.Direccion1 = direccion.Direccion1;
            editedDireccion.Direccion2 = direccion.Direccion2;
            editedDireccion.Ciudad = direccion.Ciudad;
            editedDireccion.Provincia = provincia;
            editedDireccion.Zip = direccion.Zip;
            editedDireccion.Pais = direccion.Pais;
            editedDireccion.Predeterminada = direccion.Predeterminada;
            _context.Update(editedDireccion);

           await _context.SaveChangesAsync();
        }

        public async Task<DireccionViewModel> GetDireccionAsync(int? id)
        {
            if (id == null)
            {
                return new DireccionViewModel();
            }

            var direccion = await _context.Direcciones
                .Include(t => t.TipodeDireccion)
                .Include(p => p.Provincia)
                .SingleOrDefaultAsync(x => x.Id == id);

            var provincia = direccion.Provincia;
            var provincias = await this.GetProvinciasList();

            var direccionViewModel = new DireccionViewModel
            {
                Id = (int)id,
                Nombre = direccion.Nombre,
                Direccion1 = direccion.Direccion1,
                Direccion2 = direccion.Direccion2,
                Ciudad = direccion.Ciudad,
                SelectedProvincia = provincia.Id,
                Provincias = provincias,
                Pais = direccion.Pais,
                Zip = direccion.Zip,
                Predeterminada = direccion.Predeterminada,
                TipoDeDireccion = direccion.TipodeDireccion.Tipo
            };

            return direccionViewModel;
        }

        public async Task<DireccionesViewModel> GetDirecciones(string userId)
        {
            var direcciones = new DireccionesViewModel();
            direcciones.UserId = userId;

            direcciones.TodasLasDirecciones = await _context
                .Direcciones
                .Where(x => x.UserId == userId)
                .Include(t => t.TipodeDireccion)
                .Include(p => p.Provincia)
                .ToListAsync();

            direcciones.DireccionesDestino = await GetDireccionesDestinoAsync(userId);
            direcciones.DireccionesOrigen = await GetDireccionesOrigenAsync(userId);

            return direcciones;
        }

        public async Task<List<Direccion>> GetDireccionesDestinoAsync(string userId)
        {
            var destino = await _context
                .TiposDeDireccion
                .SingleOrDefaultAsync(x => x.Tipo == "Destino");

            return await _context
                .Direcciones
                .Where(x => x.UserId == userId && x.TipodeDireccion == destino)
                .Include(t => t.TipodeDireccion)
                .Include(p => p.Provincia)
                .ToListAsync();
        }

        public async Task<List<Direccion>> GetDireccionesOrigenAsync(string userId)
        {

            var origen = await _context
                .TiposDeDireccion
                .SingleOrDefaultAsync(x => x.Tipo == "Origen");

            return await _context
                .Direcciones
                .Where(x => x.UserId == userId && x.TipodeDireccion == origen)
                .Include(t => t.TipodeDireccion)
                .Include(p => p.Provincia)
                .ToListAsync();
            
        }

        public async Task<List<SelectListItem>> GetProvinciasList()
        {
            var provincias = await _context.Provincias.Select(x => new SelectListItem
            {
                Text = x.Capital + ", " + x.NombreProvincia,
                Value = x.Id.ToString()
            }).ToListAsync();

            return provincias;
        }

        
    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models.Cargas;
using newAcargo.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Services
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _context;
        public OrderRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<OrderViewModel> ContinueUnfinishedOrder(string userId)
        {

                var borrador = await GetBorrador();
                var orderViewModel = new OrderViewModel
                {
                   Order = await _context
                    .Orders
                    .Where(x => x.Estatus == borrador && x.ShipperId == userId)
                    .Include(c => c.Cargas)
                    .Include(o => o.Origen)
                    .Include(d => d.Destino)
                    .Include(df => df.DetalleFinanciero)
                    .FirstOrDefaultAsync(),

                Empaques = await _context.TiposDeEmpaque.Select(
                    m => new SelectListItem()
                    {
                        Value = m.Id.ToString(),
                        Text = m.Empaque

                    }).ToListAsync(),

                Unidades = await _context.Unidades.Select(
                    f => new SelectListItem()
                    {
                        Value = f.Id.ToString(),
                        Text = f.UnidadDeMedida
                    }).ToListAsync()
                };

            return orderViewModel;

        }

        private async Task<EstatusOrden> GetBorrador()
        {
            return await _context
               .EstatusOrdenes
               .Where(x => x.Id == 1)
               .FirstOrDefaultAsync();
        }

        public async Task<bool> UserHasUncompletedOrder(string userId)
        {
            var borrador = await this.GetBorrador();

            return await _context
                .Orders
                .Where(x => x.ShipperId == userId && x.Estatus == borrador)
                .AnyAsync();
          
        }

        public async Task<OrderViewModel> CreateNewOrder(int subCategoryId, string userId)
        {
 
            var orderViewModel = new OrderViewModel
            {

            SubCategoria = await _context
                .ContenidosDeCarga
                .Include(d => d.Unidad)
                .Include(m => m.Empaque)
                .SingleOrDefaultAsync(x => x.Id == subCategoryId),

            Empaques = await _context.TiposDeEmpaque.Select(
                    m => new SelectListItem()
                    {
                        Value = m.Id.ToString(),
                        Text = m.Empaque

                    }).ToListAsync(),

            Unidades = await _context.Unidades.Select(
                    f => new SelectListItem()
                    {
                        Value = f.Id.ToString(),
                        Text = f.UnidadDeMedida
                    }).ToListAsync(),
            };

            orderViewModel.SelectedUnidadDeMedida = orderViewModel.SubCategoria.Unidad.Id;
            orderViewModel.SelectedEmpaque = orderViewModel.SubCategoria.Empaque.Id;

            var order = new Order();
            order.ShipperId = userId;
            order.Estatus = await this.GetBorrador();
            order.FechaCreacion = DateTime.Now;
            _context.Add(order);
            await _context.SaveChangesAsync();

            orderViewModel.Order = order;

            return orderViewModel;
        }

        public async Task<List<Carga>> GetCargasFromOrder(int orderId)
        {
            return await _context
                .Cargas
                .Where(y => y.OrderId == orderId)
                .Include(e => e.Empaque)
                .Include(t => t.Contenido)
                .ToListAsync();            
        }

       
    }

}

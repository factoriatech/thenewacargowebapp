﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Services
{
    public static class ValueServices
    {
        public static List<SelectListItem> GetProvincias()
        {
            return new List<SelectListItem>()
                {

                new SelectListItem()
                    {
                        Text = "Distrito Nacional, Santo Domingo",
                        Value = "Distrito Nacional, Santo Domingo"
                    },
                    new SelectListItem()
                    {
                        Text = "Azua, Azua de Compostela",
                        Value = "Azua, Azua de Compostela"
                    },
                    new SelectListItem()
                    {
                        Text = "Bahoruco, Neiba",
                        Value = "Bahoruco, Neiba"
                    },
                    new SelectListItem()
                    {
                        Text = "Barahona, Santa Cruz de Barahona",
                        Value = "Barahona, Santa Cruz de Barahona"
                    },
                    new SelectListItem()
                    {
                        Text = "Dajabón, Dajabón",
                        Value = "Dajabón, Dajabón"
                    },
                    new SelectListItem()
                    {
                        Text = "Duarte, San Francisco de Macorís",
                        Value = "Duarte, San Francisco de Macorís"
                    },
                    new SelectListItem()
                    {
                        Text = "Elías Piña, Comendador",
                        Value = "Elías Piña, Comendador"
                    },
                    new SelectListItem()
                    {
                        Text = "El Seibo , Santa Cruz del Seibo",
                        Value = "El Seibo , Santa Cruz del Seibo"
                    },
                    new SelectListItem()
                    {
                        Text = "Espaillat, Moca",
                        Value = "Espaillat, Moca"
                    },
                    new SelectListItem()
                    {
                        Text = "Hato Mayor, Hato Mayor del Rey",
                        Value = "Hato Mayor, Hato Mayor del Rey",

                    },
                    new SelectListItem()
                    {
                        Text = "Hermanas Mirabal, Salcedo",
                        Value = "Hermanas Mirabal, Salcedo"
                    },
                    new SelectListItem()
                    {
                        Text = "Independencia, Jimaní",
                        Value = "Independencia, Jimaní"
                    },
                    new SelectListItem()
                    {
                        Text = "La Altagracia, Salvaleón de Higüey",
                        Value = "La Altagracia, Salvaleón de Higüey"
                    },
                    new SelectListItem()
                    {
                        Text = "La Romana, La Romana",
                        Value = "La Romana, La Romana"
                    },
                    new SelectListItem()
                    {
                        Text = "La Vega, Concepción de la Vega",
                        Value = "La Vega, Concepción de la Vega"
                    },
                    new SelectListItem()
                    {
                        Text = "María Trinidad Sánchez, Nagua",
                        Value = "María Trinidad Sánchez, Nagua"
                    },
                    new SelectListItem()
                    {
                        Text = "Monseñor Nouel, Bonao",
                        Value = "Monseñor Nouel, Bonao"
                    },
                    new SelectListItem()
                    {
                        Text = "Monte Cristi, San Fernando de Monte Cristi",
                        Value = "Monte Cristi, San Fernando de Monte Cristi"
                    },
                    new SelectListItem()
                    {
                        Text = "Monte Plata, Monte Plata",
                        Value = "Monte Plata, Monte Plata"
                    },
                    new SelectListItem()
                    {
                        Text = "Monte Cristi, San Fernando de Monte Cristi",
                        Value = "Monte Cristi, San Fernando de Monte Cristi"
                    },
                    new SelectListItem()
                    {
                        Text = "Pedernales, Pedernales",
                        Value = "Pedernales, Pedernales"
                    },
                    new SelectListItem()
                    {
                        Text = "Peravia, Baní",
                        Value = "Peravia, Baní"
                    },
                    new SelectListItem()
                    {
                        Text = "Puerto Plata, San Felipe de Puerto Plata",
                        Value = "Puerto Plata, San Felipe de Puerto Plata"
                    },
                    new SelectListItem()
                    {
                        Text = "Samaná, Santa Bárbara de Samaná",
                        Value = "Samaná, Santa Bárbara de Samaná"
                    },
                    new SelectListItem()
                    {
                        Text = "Sánchez Ramírez, Cotuí",
                        Value = "Sánchez Ramírez, Cotuí"
                    },
                    new SelectListItem()
                    {
                        Text = "San Cristóbal, San Cristóbal",
                        Value = "San Cristóbal, San Cristóbal"
                    },
                    new SelectListItem()
                    {
                        Text = "San José de Ocoa, San José de Ocoa",
                        Value = "San José de Ocoa, San José de Ocoa"
                    },
                    new SelectListItem()
                    {
                        Text ="San Juan, San Juan de la Maguana",
                        Value ="San Juan, San Juan de la Maguana",
                    },
                    new SelectListItem()
                    {
                        Text = "San Pedro de Macorís, San Pedro de Macorís",
                        Value = "San Pedro de Macorís, San Pedro de Macorís"
                    },
                    new SelectListItem()
                    {
                        Text = "Santiago, Santiago de los Caballeros",
                        Value = "Santiago, Santiago de los Caballeros",
                    },
                    new SelectListItem()
                    {
                        Text = "Santiago Rodríguez, Sabaneta",
                        Value = "Santiago Rodríguez, Sabaneta"
                    },
                    new SelectListItem()
                    {
                        Text = "Santo Domingo, Santo Domingo Este",
                        Value = "Santo Domingo, Santo Domingo Este"
                    },
                    new SelectListItem()
                    {
                        Text = "Valverde, Mao",
                        Value = "Valverde, Mao"
                    }

                };

        }

        public static List<SelectListItem> GetTipoDeEmpresa()
        {
            return new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "Personal",
                        Value = "Personal"
                    },
                    new SelectListItem()
                    {
                        Text = "Empresarial",
                        Value = "Empresarial"
                    }
            };
        }

        

        public static string ImageUrl = "/Images/subcategories/";
        public static string DefaultImage = "acargoLogoDefaultImage.png";
    }
}

﻿using Google.Maps;
using Google.Maps.DistanceMatrix;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Services
{
    public class GMap : IGMap
    {
        IConfiguration configuration;
        private readonly ApplicationDbContext _context;

        public GMap(IConfiguration configuration, ApplicationDbContext context)
        {
            this.configuration = configuration;
            _context = context;
        }

        public DistanceMatrixService CreateService()
        {
            var apiKey = configuration.GetValue<string>("GoogleApi:ApiKey");
            var TestingApiKey = new GoogleSigned(apiKey);
            var svc = new DistanceMatrixService(TestingApiKey);
            return svc;
        }

        public async Task<string> GetDistance(string origen, string destino)
        {
            DistanceMatrixRequest request = new DistanceMatrixRequest();
            request.AddDestination(new Location(destino));
            request.AddOrigin(new Location(origen));
            request.Mode = TravelMode.driving;
            DistanceMatrixResponse response = await CreateService().GetResponseAsync(request);

            return response.Rows.First().Elements.First().distance.Text;
        }

        public async Task<string> GetOrderDistance(int orderId)
        {
            var order = await _context
                .Orders
                .Include(y => y.Origen)
                .Include(z => z.Destino)
                .FirstOrDefaultAsync(x => x.Id == orderId);

            Direccion DireccionOrigen = order.Origen;
            Direccion DireccionDestino = order.Destino;
            var origen = DireccionOrigen.Ciudad + " " + DireccionOrigen.Provincia;
            var destino = DireccionDestino.Ciudad + " " + DireccionDestino.Provincia;
            return await GetDistance(origen, destino);
        }

        public async Task<double> GetDistanceDouble(string origen, string destino)
        {
            DistanceMatrixRequest request = new DistanceMatrixRequest();
            request.AddDestination(new Location(destino));
            request.AddOrigin(new Location(origen));
            request.Mode = TravelMode.driving;
            DistanceMatrixResponse response = await CreateService().GetResponseAsync(request);
            var dist = response.Rows.First().Elements.First().distance.Value;
            return dist / 1000;
        }

        public async Task<string> GetDuration(string origen, string destino)
        {
            DistanceMatrixRequest request = new DistanceMatrixRequest();
            request.AddDestination(new Location(destino));
            request.AddOrigin(new Location(origen));
            request.Mode = TravelMode.driving;
            DistanceMatrixResponse response = await CreateService().GetResponseAsync(request);
            return response.Rows.First().Elements.First().duration.Text;
        }

        public async Task<string> GetOrderDuration(int orderId)
        {
            var order = await _context
                .Orders
                .Include(y => y.Origen)
                .Include(z => z.Destino)
                .FirstOrDefaultAsync(x => x.Id == orderId);

            Direccion DireccionOrigen = order.Origen;
            Direccion DireccionDestino = order.Destino;
            var origen = DireccionOrigen.Ciudad + " " + DireccionOrigen.Provincia;
            var destino = DireccionDestino.Ciudad + " " + DireccionDestino.Provincia;
            return await GetDuration(origen, destino);

        }

        public async Task<double> GetOrderDistanceDouble(int orderId)
        {
            var order = await _context
                .Orders
                .Include(y => y.Origen)
                .Include(z => z.Destino)
                .FirstOrDefaultAsync(x => x.Id == orderId);

            Direccion DireccionOrigen = order.Origen;
            Direccion DireccionDestino = order.Destino;
            var origen = DireccionOrigen.Ciudad + " " + DireccionOrigen.Provincia;
            var destino = DireccionDestino.Ciudad + " " + DireccionDestino.Provincia;
            return await this.GetDistanceDouble(origen, destino);
        }
    }
}

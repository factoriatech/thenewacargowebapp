﻿using Microsoft.AspNetCore.Identity;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models
{
    public class ApplicationUser: IdentityUser
    {
        public ApplicationUser() : base() { }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TelefonoOficina { get; set; }
        public string Extension { get; set; }
        public string Cedula { get; set; }
        public string Posicion { get; set; }
        public Direccion Direccion { get; set; }
        public string Tipo { get; set; }
        public string TelefonoCelular { get; set; }
        public string Empresa { get; set; }
        public string RNC { get; set; }
        public NCFTipo NCF { get; set; }
    }
}

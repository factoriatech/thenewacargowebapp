﻿using System;
namespace newAcargo.Models.Direcciones
{
    public class Provincia
    {
        public int Id { get; set; }
        public string Capital { get; set; }
        public string NombreProvincia { get; set; }
    }
}

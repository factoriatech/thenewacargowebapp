﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Direcciones
{
    public class DireccionViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Dale un nombre a tu Direccion")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "{0} es Obligatorio")]
        [Display(Name = "Direccion 1")]
        public string Direccion1 { get; set; }

        [Display(Name = "Direccion 2")]
        public string Direccion2 { get; set; }

        public string Ciudad { get; set; }

        public int SelectedProvincia { get; set; }
        public List<SelectListItem> Provincias { get; set; }

        [Display(Name = "Codigo Postal")]
        public string Zip { get; set; }

        public string Pais { get; set; }

        [Display(Name = "Direccion Predetrminada")]
        public bool Predeterminada { get; set; }
        public string TipoDeDireccion { get; set; }
        
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Direcciones
{
    public class Direccion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion1 { get; set; }
        public string Direccion2 { get; set; }
        public string Ciudad { get; set; }
        public Provincia Provincia { get; set; }
        public string Zip { get; set; }
        public string Pais { get; set; }
        public bool Predeterminada { get; set; }
        public TipoDeDireccion TipodeDireccion { get; set; }
        public string UserId { get; set; }
    }
}

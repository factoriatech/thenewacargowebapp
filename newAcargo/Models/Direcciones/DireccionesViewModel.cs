﻿using newAcargo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Direcciones
{
    public class DireccionesViewModel
    {
        public string UserId { get; set; }
        public List<Direccion> DireccionesOrigen { get; set; }
        public List<Direccion> DireccionesDestino { get; set; }
        public List<Direccion> TodasLasDirecciones { get; set; }

        
    }
}

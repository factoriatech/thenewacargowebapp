﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Direcciones
{
    public class TipoDeDireccion
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class FormulaResult
    {

        public double TotalWeight { get; set; }
        public double TotalVolume { get; set; }
        public double TotalPesoVolumen { get; set; }
        public decimal TotalFf { get; set; }

        public decimal SubtotalSinFtl { get; set; }
        public string SubtotalSinFtlMoney
        {
            get { return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", SubtotalSinFtl); }
        }
      
        public decimal SubtotalConFtl { get; set; }
        public string SubtotalConFtlMoney
        {
            get { return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", SubtotalConFtl); }
        }

        public decimal Distance { get; set; }
        public decimal VolumenByDistance { get; set; }
        public FtlResult FullTruckLoad { get; set; }
      
        public decimal Subtotal { get; set; }
        public string SubTotalMoney
        {
            get { return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", Subtotal); }
        }




    }
}

﻿using System;
using newAcargo.Models.Direcciones;

namespace newAcargo.Models.Calculations
{
    public class FactorProvincia
    {
        public int Id { get; set; }
        public double Origen { get; set; }
        public double Destino { get; set; }

        public int ProvinciaId { get; set; }
        public Provincia Provincia { get; set; }

    }
}

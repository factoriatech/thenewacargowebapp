﻿using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class FtlCalculations: IFtlCalculations
    {
        private readonly ApplicationDbContext _context;

        public FtlCalculations(ApplicationDbContext context)
        {
            _context = context;
        }

        private async Task<double> GetMinCF()
        {

            return await _context.FullTruckLoads.MinAsync(x => x.ValorDesde);
        }

        private async Task<double> GetMaxWeight()
        {
            return await _context.FullTruckLoads.MaxAsync(x => x.PesoMaximo);
        }

        private async Task<double> GetMaxCF()
        {
            return await _context.FullTruckLoads.MaxAsync(x => x.ValorHasta);
        }


        public async Task<decimal> GetFtlPrice(double distance, double peso, double volumen)
        {
            var maxWeight = await this.GetMaxWeight();
            var maxVol = await this.GetMaxCF();
            var minVol = await this.GetMinCF();

            if( (minVol > volumen) || (maxVol < volumen) || (maxWeight < peso))
            {
                return 0;
            }

            var ftl = new FullTruckLoad();
            var any = await _context
                .FullTruckLoads.Where(x => x.PesoMaximo > peso && 
                x.ValorDesde < volumen && 
                x.ValorHasta >= volumen)
                .AnyAsync();

            if (any)
            {
                ftl = await _context
                .FullTruckLoads.Where(x => x.PesoMaximo > peso &&
                x.ValorDesde < volumen &&
                x.ValorHasta >= volumen)
                .FirstOrDefaultAsync();
            }

            return ftl.PrecioPorKm*(decimal)distance;        
        }

        public async Task<FtlResult> GetFtl(double distance, double peso, double volumen)
        {
            var maxWeight = await this.GetMaxWeight();
            var maxVol = await this.GetMaxCF();
            var minVol = await this.GetMinCF();
            var ftl = new FtlResult();
            ftl.Ftl = new FullTruckLoad();

            if(minVol > volumen)
            {
                ftl.Reason = "El envio no llega a tener el volumen Minimo para un FTL";
                return ftl;
            }

            if(maxVol < volumen)
            {
                ftl.Reason = "No hay una tarifa de FTL configurado para un volumen tan alto";
                return ftl;
            }

            if(maxWeight < peso)
            {
                ftl.Reason = "No hay una tarifa de FTL configurado para un peso tan alto";
                return ftl;
            }

            var any = await _context
                .FullTruckLoads.Where(x => x.PesoMaximo > peso &&
                x.ValorDesde < volumen &&
                x.ValorHasta >= volumen)
                .AnyAsync();

            if (any)
            {
                ftl.Ftl = await _context
                .FullTruckLoads.Where(x => x.PesoMaximo > peso &&
                x.ValorDesde < volumen &&
                x.ValorHasta >= volumen)
                .FirstOrDefaultAsync();
            }

            return ftl;
        }
    }
}

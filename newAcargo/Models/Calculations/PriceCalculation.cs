﻿using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using newAcargo.Models.Cargas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class PriceCalculation : IPriceCalculation
    {
        private readonly ApplicationDbContext _context;
        private readonly IGMap _gmap;
        private readonly IFtlCalculations _ftl;

        public PriceCalculation(ApplicationDbContext context, IGMap gmap, IFtlCalculations ftl)
        {
            _context = context;      
            _gmap = gmap;
            _ftl = ftl;
        }

        private async Task<CalculationParameterSet> GetActivePriceParams()
        {
            return await _context
                .CalculationParameterSets
                .Where(x => x.Active == true)
                .FirstOrDefaultAsync();
        }

        public Task<decimal> CalculateComision(int orderId)
        {
            throw new NotImplementedException();
        }

        private async Task<decimal> GetFactorDeFrecuencia(int origenId, int destinoId)
        {
            var factorOrigen = await _context.FactoresProvincias.SingleOrDefaultAsync(x => x.Id == origenId);
            var factorDestino = await _context.FactoresProvincias.SingleOrDefaultAsync(x => x.Id == destinoId);

            return (decimal)factorDestino.Destino + (decimal)factorOrigen.Origen;
        }

        private async Task<FactorProvincia> GetFactorOrigen(int origenId)
        {
            return await _context.FactoresProvincias.SingleOrDefaultAsync(x => x.Id == origenId);
        }

        private async Task<FactorProvincia> GetFactorDestino(int destinoId)
        {
            return await _context.FactoresProvincias.SingleOrDefaultAsync(x => x.Id == destinoId);
        }

        public async Task<decimal> GetOrderPrice(int orderId)
        {
            var order = await _context
                .Orders
                .Include(c => c.Cargas)
                .Include(o => o.Origen)
                .Include(d => d.Destino)
                .FirstOrDefaultAsync();
            var origen = order.Origen;
            var destino = order.Destino;
            var distance = await _gmap.GetOrderDistanceDouble(orderId);

            return await this.GetPrice(order.Cargas, distance, origen.Id, destino.Id);
        }


        private async Task<double> GetOrderTotalWeight(int orderId)
        {
            var cargas = await _context
                .Cargas
                .Where(x => x.OrderId == orderId)
                .ToListAsync();
            double total = 0;

            foreach(var t in cargas)
            {
                total += t.Peso;
            }

            return total;
        }

        private async Task<double> GetOrderVolume(int orderId)
        {
            var cargas = await _context
                .Cargas
                .Where(x => x.OrderId == orderId)
                .ToListAsync();

            double total = 0;

            foreach (var t in cargas)
            {
                total += t.Profundidad*t.Alto*t.Ancho;
            }

            return total;
        }

        private async Task<double> GetOrderVolumeWeight(int orderId)
        {
            var cargas = await _context
                .Cargas
                .Where(x => x.OrderId == orderId)
                .ToListAsync();
            double total = 0;

            foreach (var t in cargas)
            {
                total += t.PesoVolumen;
            }

            return total;
        }

        private double GetCargasVolume(List<Carga> cargas)
        {
            double total = 0;

            foreach(var c in cargas)
            {
                total += c.Alto * c.Ancho * c.Profundidad;
            }

            return total;
        }

        private double GetCargasWeight(List<Carga> carga)
        {
            double total = 0;

            foreach(var c in carga)
            {
                total += c.Peso;
            }

            return total;
        }

        private double GetCargasPesoVolumen(List<Carga> carga)
        {
            double total = 0;

            foreach (var c in carga)
            {
                total += c.PesoVolumen;
            }

            return total;
        }


        public async Task<decimal> GetPrice(List<Carga> cargas, double distance, int origen, int destino)
        {
            //buscar el set de parametros activo
            var _params = await this.GetActivePriceParams();

            ///verificar el minimo de distancia de acuerdo al set de parametros
            var dist = (decimal)distance;

            if(dist < _params.MinimumDistance)
            {
                dist = _params.MinimumDistance;
            }

            //buscar totales de volumen, peso y pesoVolumen
            var totalVolume = this.GetCargasVolume(cargas);
            var totalWeight = this.GetCargasWeight(cargas);
            var totalPesoVolumen = this.GetCargasPesoVolumen(cargas);

            //buscar el registro de origen y destino de la tabla de factores de provincia y calcular el factor de frecuencia
            decimal ff = await this.GetFactorDeFrecuencia(origen, destino);

            //Hacer la formula (pesovol * distancia/factor divisor)*factor de frecuencia
            decimal VolumenPorDistancia = ((decimal)totalPesoVolumen * dist) / _params.FactorDivisor;
            decimal subtotal = (decimal)ff * VolumenPorDistancia;

            //buscar ahora el valor de este listado de cargas y distancia pero con un FTL
            var ftlPrice = await _ftl.GetFtlPrice( (double)dist, totalWeight, totalVolume );
            var totalFtl = ftlPrice * ff;

            if((totalFtl < subtotal) && (totalFtl > 0))
            {
                return totalFtl;
            }

            if( subtotal < _params.MinimumPrice)
            {
                return _params.MinimumPrice;
            }

            return subtotal;
        }

        public async Task<decimal> GetPriceQuote(CargaPriceCalculatorBinding cargaPriceCalculatorBinding, double distance)
        {
            //buscar el set de parametros activo
            var _params = await this.GetActivePriceParams();

            ///verificar el minimo de distancia de acuerdo al set de parametros
            var dist = (decimal)distance;

            if (dist < _params.MinimumDistance)
            {
                dist = _params.MinimumDistance;
            }

            //buscar totales de volumen, peso y pesoVolumen
            var totalVolume = cargaPriceCalculatorBinding.Dimensiones;
            var totalWeight = cargaPriceCalculatorBinding.Peso;
            var totalPesoVolumen = cargaPriceCalculatorBinding.PesoVolumen;

            //buscar el registro de origen y destino de la tabla de factores de provincia y calcular el factor de frecuencia
            decimal ff = await this.GetFactorDeFrecuencia(cargaPriceCalculatorBinding.ProvinciaOrigen, cargaPriceCalculatorBinding.ProvinciaDestino);

            //Hacer la formula (pesovol * distancia/factor divisor)*factor de frecuencia
            decimal VolumenPorDistancia = ((decimal)totalPesoVolumen * dist) / _params.FactorDivisor;
            decimal subtotal = (decimal)ff * VolumenPorDistancia;

            //buscar ahora el valor de este listado de cargas y distancia pero con un FTL
            var ftlPrice = await _ftl.GetFtlPrice((double)dist, totalWeight, totalVolume);

            var totalFtl = ftlPrice * ff;

            if (totalFtl < subtotal && totalFtl > 0)
            {
                return totalFtl;
            }

            if (subtotal < _params.MinimumPrice)
            {
                return _params.MinimumPrice;
            }

            return subtotal;
        }

        public async Task<FormulaResult> GetFormulaResult(CargaPriceCalculatorBinding cargaPriceCalculatorBinding, double distance)
        {
            FormulaResult output = new FormulaResult();
            var _params = await this.GetActivePriceParams();

            output.Subtotal = await this.GetPriceQuote(cargaPriceCalculatorBinding, distance);
            output.TotalFf = await this.GetFactorDeFrecuencia(cargaPriceCalculatorBinding.ProvinciaOrigen, cargaPriceCalculatorBinding.ProvinciaDestino);
            output.TotalPesoVolumen = cargaPriceCalculatorBinding.PesoVolumen;
            output.TotalVolume = cargaPriceCalculatorBinding.Dimensiones;
            output.TotalWeight = cargaPriceCalculatorBinding.Peso;

            var dist = (decimal)distance;

            if (dist < _params.MinimumDistance)
            {
                dist = _params.MinimumDistance;
            }

            output.Distance = dist;
            output.VolumenByDistance = ((decimal)output.TotalPesoVolumen * dist) / _params.FactorDivisor;
            output.SubtotalSinFtl = output.VolumenByDistance * output.TotalFf;
            output.FullTruckLoad = await _ftl.GetFtl((double)dist, output.TotalWeight, output.TotalVolume);
            output.SubtotalConFtl = output.FullTruckLoad.Ftl.PrecioPorKm * output.TotalFf * dist;

            return output;
        }
    }

}

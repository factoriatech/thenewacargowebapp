﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class QuoteValues
    {
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal SubTotal { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal CargoRecogida { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal CargoEntrega { get; set; }

        public double Distance { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class FtlResult
    {
        public string Reason { get; set; }
        public FullTruckLoad Ftl { get; set; }
    }
}

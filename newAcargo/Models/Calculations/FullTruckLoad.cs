﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class FullTruckLoad
    {
        public int Id { get; set; }
        public double ValorDesde { get; set; }
        public double ValorHasta { get; set; }
        public double PesoMaximo { get; set; }
        public decimal PrecioPorKm { get; set; }
    }
}

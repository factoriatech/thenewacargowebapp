﻿using Microsoft.AspNetCore.Mvc.Rendering;
using newAcargo.Models.Cargas;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class PriceCalculatorViewModel
    {
        public int Id { get; set; }
        public List<Carga> Cargas { get; set; }
        public double Distance { get; set; }

        public int SelectedOrigen { get; set; }
        public List<SelectListItem> Origen { get; set; }

        public int SelectedDestino { get; set; }
        public List<SelectListItem> Destino { get; set; }

        [Display(Name = "Selecciona Tipo de Empaque")]
        public int SelectedEmpaqueId { get; set; }
        public List<SelectListItem> Empaques { get; set; }



    }
}

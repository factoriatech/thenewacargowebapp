﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class CargaPriceCalculatorBinding
    {
        public double Peso { get; set; }
        public double PesoVolumen { get; set; }
        public double Dimensiones { get; set; }
        public int ProvinciaOrigen { get; set; }
        public int ProvinciaDestino { get; set; }

    }
}

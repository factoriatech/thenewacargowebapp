﻿using Microsoft.EntityFrameworkCore;
using newAcargo.Data;
using newAcargo.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Calculations
{
    public class CalculationParameterSet
    {
       
        public int Id { get; set; }


        [DisplayName("Escribe un nombre para este set de parametrizacion ")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public string Nombre { get; set; }

        [DisplayName("Es el denominador de la formula de Volumen por Distancia: PesoVolumen X Distancia/ (Este valor)")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal FactorDivisor { get; set; }

        [DisplayName("Precio fijo de Recogida")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal PricePickup { get; set; }

        [DisplayName("Precio fijo de Entrega")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal PriceDelivery { get; set; }

        [DisplayName("Porcentaje de Comision de aCargo")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal ComisionPorcentage { get; set; }

        [DisplayName("Monto total minimo para una orden")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal MinimumPrice { get; set; } 

        [DisplayName("Distancia Minima para una orden")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal MinimumDistance { get; set; }


        [DisplayName("Sirve para dividir el Peso Volumen dentro de la formula")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal VolumeWeightDivisor { get; set; }

        [DisplayName("Precio total por KM de un FTL")]
        [Required(ErrorMessage = "Todos los campos son Requeridos")]
        public decimal FtlPerKm { get; set; }

        public bool Active { get; set; }
        public bool Protected { get; set; }
    }
}

﻿using System;
namespace newAcargo.Models.FrontEndUI
{
    public class SubcategoryListViewModel
    {
            public int Id { get; set; }
            public string Contenido { get; set; }
            public string Categoria { get; set; }
            public double Ancho { get; set; }
            public double Alto { get; set; }
            public double Profundidad { get; set; }
            public double Peso { get; set; }
    }
}

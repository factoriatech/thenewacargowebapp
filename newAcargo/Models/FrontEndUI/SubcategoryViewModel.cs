using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using newAcargo.Models.Cargas;
using newAcargo.Services;

namespace newAcargo.Models.FrontEndUI
{
    public class SubcategoryViewModel
    {
        public int Id { get; set; }
        
        [Required]
        [Display(Name = "Nombre del Contenido (Subcategoria)")]
        public string Contenido { get; set; }

        [Display(Name = "Imagen(Tamaño ideal: 208px x 208px)")]
        public string Image { get; set; } = ValueServices.DefaultImage;
        public IFormFile ImageFile {get; set;}


        public double Ancho { get; set; }
        public double Alto { get; set; }
        public double Profundidad { get; set; }
        public double Peso { get; set; }

        [Display(Name = "Unidad de Medida")]
        public int SelectedUnidadId { get; set; }
        public List<SelectListItem> Unidades { get; set; }

        [Display(Name = "Tipo de Empaque")]
        public int SelectedEmpaque { get; set; }
        public List<SelectListItem> Empaques { get; set; }

        [Display(Name = "Categoria")]
        public int SelectedCategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }

    }
}
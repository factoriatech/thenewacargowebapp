﻿using newAcargo.Models.Cargas;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.FrontEndUI
{
    public class ShippingCategory
    {
        public ShippingCategory()
        {
            Subcategorias = new List<ContenidoDeCarga>();
            
        }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Publicado { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public IList<ContenidoDeCarga> Subcategorias { get; set; }
    }
}

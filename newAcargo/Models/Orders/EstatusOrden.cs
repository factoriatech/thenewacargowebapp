﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Orders
{
    public class EstatusOrden
    {
        public int Id { get; set; }
        public string Estatus { get; set; }
    }
}

//Workflow
//1. Borrador - no ha pagado
//2. En Proceso - pago y nadie la ha cogido
//3. Programada - la cogio un carrier y no la ha asigando a un chofer
//4. Asignada - Se asigno a un chofer
//4. En Ruta - La pasaron a buscar ya (se activa google maps)
//5. Completada - Se entrego la orden
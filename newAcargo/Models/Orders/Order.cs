﻿using newAcargo.Models.Cargas;
using newAcargo.Models.Direcciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Orders
{
    public class Order
    {
        public int Id { get; set; }
        public EstatusOrden Estatus { get; set; }

        public Direccion Origen { get; set; }
        public Direccion Destino { get; set; }

        public DateTime FechaCreacion { get; set; }
        public DateTime FechaEntrega { get; set; }

        public Seguro Seguro { get; set; }
        public decimal CostoMercancia { get; set; }

        public DatosDelEnvio DatosDelEnvio { get; set; }
        public List<Carga> Cargas { get; set; }

        public double Distance { get; set; }
        public TimeSpan Duration { get; set; }

        public string ShipperId { get; set; }
        public string CarrierId { get; set; }
        public string ChoferId { get; set; }
        public DetalleFinanciero DetalleFinanciero { get; set; }
    }
}

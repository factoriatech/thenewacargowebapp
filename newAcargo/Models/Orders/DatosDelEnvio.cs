﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Orders
{
    public class DatosDelEnvio
    {
        public int Id { get; set; }
        public ContactoDeEnvio ContactoRecogida { get; set; }
        public ContactoDeEnvio Entrega { get; set; }
        public DateTime FechaInicialRecogida { get; set; }
        public DateTime FechaFinalRecogida { get; set; }
        public string HorarioRecogida { get; set; }

        public bool RampaRecogida { get; set; }
        public bool LLamarRecogida { get; set; }
        public bool SolicitarAccesoRecogida { get; set; }
        public string HorarioDeRecogida { get; set; }

        public string DatosContactoEntrega { get; set; }
        public bool RampaEntrega { get; set; }
        public bool LLamarEntrega { get; set; }
        public bool SolicitarAccesoEntrega { get; set; }

        public Order Order { get; set; }
        public int OrderId { get; set; }


    }
}

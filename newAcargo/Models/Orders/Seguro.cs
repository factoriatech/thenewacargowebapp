﻿namespace newAcargo.Models.Orders
{
    public class Seguro
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Margen { get; set; }

    }
}
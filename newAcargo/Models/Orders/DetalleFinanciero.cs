﻿namespace newAcargo.Models.Orders
{
    public class DetalleFinanciero
    {
        public int Id { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Itbis { get; set; }
        public decimal IngresoCarrier { get; set; }
        public decimal ComisionAcargo { get; set; }
        public decimal Total { get; set; }

        public Order Order { get; set; }
        public int OrderId { get; set; }
    }
}
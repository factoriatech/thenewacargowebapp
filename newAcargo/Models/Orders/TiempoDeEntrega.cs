﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Orders
{
    public class TiempoDeEntrega
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int CtdDeHoras { get; set; }
        public decimal Incremento { get; set; }

    }
}

﻿namespace newAcargo.Models.Cargas
{
    public class TipoDeEmpaque
    {
        public int Id { get; set; }
        public string Empaque { get; set; }
    }
}
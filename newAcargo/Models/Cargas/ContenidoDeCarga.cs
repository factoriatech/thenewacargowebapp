﻿using newAcargo.Models.FrontEndUI;
using newAcargo.Services;
using System.ComponentModel.DataAnnotations;

namespace newAcargo.Models.Cargas
{
    public class ContenidoDeCarga
    {

        public int Id { get; set; }
        public string Contenido { get; set; }
        public string Image { get; set; }

        public double Ancho { get; set; }
        public double Alto { get; set; }
        public double Profundidad { get; set; }

        public double Peso { get; set; }
        public Unidad Unidad { get; set; }

        public int ShippingCategoryId { get; set; }
        public ShippingCategory ShippingCategories { get; set; }
        public TipoDeEmpaque Empaque { get; set; }
        public bool IsAModel {get;set;}
    }
}
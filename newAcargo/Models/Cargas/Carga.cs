﻿using newAcargo.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models.Cargas
{
    public class Carga
    {
        public int Id { get; set; }
        public ContenidoDeCarga Contenido { get; set; }

        public double Profundidad { get; set; }
        public double Ancho { get; set; }
        public double Alto { get; set; }

        public double Peso { get; set; }
        public int Cantidad { get; set; }
        public Unidad Unidad { get; set; }
        public bool Apilable { get; set; }
        public TipoDeEmpaque Empaque { get; set; }
        public double PesoVolumen { get; set; }

        public Order Order { get; set; }
        public int OrderId { get; set; }







    }
}

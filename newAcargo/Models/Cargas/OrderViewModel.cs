﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using newAcargo.Models.Direcciones;
using newAcargo.Models.Orders;

namespace newAcargo.Models.Cargas
{
    public class OrderViewModel
    {
       
        public int Id { get; set; }
        public ContenidoDeCarga SubCategoria { get; set; }

        public List<SelectListItem> Empaques { get; set; }

        [Display(Name = "Empaque")]
        public int SelectedEmpaque { get; set; }

        public List<SelectListItem> Unidades { get; set; }
        [Display(Name = "Unidad de Medida")]
        public int SelectedUnidadDeMedida { get; set; }

        public Order Order { get; set; }

        public int SelectedDireccionOrigen { get; set; }
        public List<SelectListItem> DireccionesDeOrigen { get; set; }

        public int SelectedDireccionDestino { get; set; }
        public List<SelectListItem> DireccionesDeDestino { get; set; }

    }
}

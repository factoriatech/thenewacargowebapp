﻿namespace newAcargo.Models.Cargas
{
    public class Unidad
    {
        public int Id { get; set; }
        public string UnidadDeMedida { get; set; }
    }
}
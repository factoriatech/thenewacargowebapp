﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newAcargo.Models
{
    public class NCFTipo
    {
        public int Id { get; set; }
        public string TipoDeComprobante { get; set; }
    }
}
